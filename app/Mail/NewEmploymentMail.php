<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewEmploymentMail extends Mailable
{
    use Queueable, SerializesModels;
    public $first_name;
    public $last_name;
    public $link;
    public $download_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($first_name, $last_name, $download_link, $link)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->link = $link;
        $this->download_link = $download_link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('I-9 Employment Form')->cc(['admin@aastaffing.org'])->view('mails.employment-form');
    }
}
