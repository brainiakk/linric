<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function sendMail(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'msg' => 'required|string',
        ],
            [
                // 'subject.regex'  => 'We do not allow suspicious characters in our inquiry box, please use plain English, Thanks.',
                'msg.required'  => 'Message box should be properly filled',
//                'msg.regex'  => 'We do not allow suspicious characters in our inquiry box, please use plain English, Thanks.',
            ]);

        if ($validator->fails()) {
            return redirect('/contact')
                ->withErrors($validator)
                ->withInput();
        }

        $name = $request->name;
        $email = $request->email;
        $msg = strip_tags($request->msg);
//        $mailData = [
//            'name' => $name,
//            'email' => $email,
//            'msg' => $msg,
//        ];


        $mail = Mail::to('info@linricproservices.com')->send(new ContactMail($name, $email, $msg));

        if (!$mail){
            if($request->ajax()){
                return response()->json(['msg' => 'Email was not sent!!'], 203);
            }
            Session::flash('error', "Email was not sent!!");
            return Redirect::back();
        } else {
            if($request->ajax()){
                return response()->json(['msg' => 'Email was sent successfully!!'], 200);
            }
            Session::flash('success', "Email was sent successfully!!");
            return Redirect::back();
        }
    }
}
