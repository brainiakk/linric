<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $message = array(
            'fname.required' => 'Please enter Your First Name!!',
            'lname.required' => 'Please enter Your Last Name!!',
            'email.required' => 'Please enter a Valid Email Address!!',
            'tel.required' => 'Please enter a Valid Phone Number!!',
            'password.required' => 'Password is Mandatory!!',
            'gender.required' => 'Please Select Your Gender!!',
        );
        return Validator::make($data, [
            'fname' => ['required', 'string', 'max:255', 'min:3'],
            'lname' => ['required', 'string', 'max:255', 'min:3'],
            'tel' => ['required', 'string', 'max:255', 'min:6', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'gender' => ['required', 'string'],
        ], $message);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'fname' => $data['fname'],
            'mname' => $data['mname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'gender' => $data['gender'],
            'tel' => $data['tel'],
            // 'type' => $data['type'],//How did you hear about Us
            'level' => 0,
            'status' => 1,
            'password' => Hash::make($data['password']),
        ]);
    }
}
