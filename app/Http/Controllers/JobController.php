<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\JobApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = \Carbon\Carbon::now()->format('Y-m-d');
        // $jobs = Job::whereStatus('1')->where('close_date', '>', $now)->orderBy('updated_at', 'DESC')->simplePaginate(6);
        $jobs = Job::active()->orderBy('updated_at', 'DESC')->paginate(20);
        return view('jobs.index', compact('jobs'));
    }


    public function applied()
    {
        $applied = JobApplication::orderBy('updated_at', 'DESC')->whereUser_id(\Auth::id())->paginate(20);
        return view('users.applied', compact('applied'));
    }

    public function jobAppDestroy(Request $request, JobApplication $jobApplication)
    {

        if(\Auth::user()->isOwner($jobApplication->user_id)){
            if(\File::exists(public_path($jobApplication->resume))){
                \File::delete(public_path($jobApplication->resume));
            }
            $destroy = $jobApplication->delete();
            if($destroy){
                if($request->ajax()){
                    return response()->json(['msg' => 'Job Application Deleted Successfully!!'], 200);
                }
                Session::flash('success', "Job Application Deleted Successfully!!");
                return Redirect::back();
            }else{
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Job Application Failed To Delete.'], 422);
                }
                Session::flash('error', "Oops!! Job Application Failed To Delete.");
                return Redirect::back();
            }
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Unauthorized Delete Action.'], 422);
            }
            Session::flash('error', "Oops!! Unauthorized Delete Action.");
            return Redirect::back();
        }
    }


    public function searchJobs(Request $request)
    {
        $now = \Carbon\Carbon::now()->format('Y-m-d');
        if ($request->filter)
        {
            $jobs = Job::available()->where('title', 'LIKE', "%{$request->filter}%")->orWhereHas('category', function($q) use($request){
                $q->where('cat_name', 'LIKE', "%{$request->filter}%");
            })->paginate(15);
            $query = $request->filter;
            return view('jobs.index', compact('jobs', 'query'));
        }
        elseif($request->search){
            $jobs = Job::available()
                ->when(!empty($request->get('cat_id')) , function ($query) use($request){
                    return $query->where('cat_id', $request->cat_id);
                })
                ->when(!empty($request->get('type_id')) , function ($query) use($request){
                    return $query->where('type_id', $request->type_id);
                })
                ->when (!empty($request->input('search')) , function ($query) use($request){
                    return $query->where('title', 'LIKE', "%{$request->search}%");
                })
                ->when (!empty($request->input('location')) , function ($query) use($request){
                    return $query->where('location', 'LIKE', "%{$request->location}%");
                })
                ->when (!empty($request->rate) , function ($query) use($request){
                    if ($request->rate == '200')
                    {
                        return $query->where('wages', '>=', 200);
                    }
                    else
                    {
                        return $query->whereBetween('wages', $request->rate);
                    }
                })
                ->when(!empty($request->get('sort')) , function ($query) use($request){
                   if ($request->sort == 'oldest'){
                       return $query->orderBy("created_at", "asc");
                   }elseif ($request->sort == 'ratehigh'){
                       return $query->orderBy("wages", "desc");
                   }elseif ($request->sort == 'ratelow'){
                        return $query->orderBy("wages", "asc");
                    }else{
                        return $query->orderBy("created_at", "desc");
                    }
                })->paginate(15);
            $query = $request->search;
            return view('jobs.index', compact('jobs', 'query'));
        }else{
            Session::flash('error', "Oops!! Search box is empty.");
            return Redirect::back();
        }
    }

    public function searchCatJobs(Request $request)
    {
        if ($request->filter)
        {
            $jobs = Job::available()->where('title', 'LIKE', "%{$request->filter}%")->orWhereHas('category', function($q) use($request){
                $q->where('cat_name', 'LIKE', "%{$request->filter}%");
            })->paginate(15);
            return view('jobs.index', compact('jobs', 'query'));
        }
        else
        {
            return Redirect::route('jobs.homr');
        }
    }


    public function jobApply(Job $job, Request $request)
    {
        // dd($job);
        if (JobApplication::where('user_id', \Auth::id())->where('job_id', $job->id)->first()) {
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! You Already Applied For This Job.'], 422);
            }
            Session::flash('error', "Oops!! You Already Applied For This Job.");
            return Redirect::back();
        }else{
            $apply = new JobApplication();
            $apply->user_id = \Auth::id();
            $apply->job_id = $job->id;
            $apply->company_id = 1;
            $apply->status = '1';
            $apply->cover_letter = $request->cover_letter;
            if ($request->hasfile('resume')) {
                $file = $request->file('resume');
                $filename = \Str::slug(\Auth::user()->name()).'-resume-'.time().'.'.$file->extension();
                $file->move(public_path().'/uploads/employee_documents/resume/', $filename);
                $apply->resume = '/uploads/employee_documents/resume/'.$filename;
            }
            if ($apply->save()) {
                if($request->ajax()){
                    return response()->json(['msg' => 'Job Application Successfully!!'], 200);
                }
                Session::flash('success', "Job Application Successfully!!");
                return Redirect::back();
            } else {
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Job Application Failed.'], 422);
                }
                Session::flash('error', "Oops!! Job Application Failed.");
                return Redirect::back();
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        //
    }
}
