<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employee;
use App\Models\Job;
use App\Models\JobApplication;
use App\Models\JobAssigned;
use App\Models\JobCategory;
use App\Models\User;
use App\Models\Visitor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function userProfile(User $user){
        if(Gate::denies('admin')) {


            return redirect()->route('dashboard.profile.user');
        }else{
            return view('admin.users.create', compact('user'));
        }
    }

    public function index()
    {
        if (Gate::denies('admin')) {
            $num_apps = count(JobApplication::owner()->get());
            $num_active_apps = count(JobApplication::owner()->active()->get());
            $num_active_jobs = count(Job::active()->get());
            return view('users.index', compact('num_apps', 'num_active_apps', 'num_active_jobs'));
        }

        return redirect()->route('dashboard.admin.home');
    }

    public function adminHome ()
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $date = date('Y-m-d');
        $num_users = count(User::all());
        $num_jobs = count(Job::all());
        $num_apps = count(JobApplication::all());
        $num_active_jobs = count(Job::active()->get());
        $num_visitors = Visitor::where('date', $date)->first()->views;
        return view('admin.index', compact('num_apps','num_users', 'num_active_jobs', 'num_visitors', 'num_jobs'));
    }

    public function category()
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $cats = JobCategory::latest()->paginate(20);
        return view('admin.jobs.categories', compact('cats'));
    }

    public function categoryAdd(JobCategory $jobCategory)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }

        return view('admin.jobs.create-category', compact('jobCategory'));
    }

    public function categorySave(Request $request)
    {
        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }

        $input = $request->all();
        $save_cat = JobCategory::create($input);
        if($save_cat){
            if($request->ajax()){
                return response()->json(['msg' => 'Category Saved Successfully!!'], 200);
            }
            Session::flash('success', "Category Saved Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Category Failed To Save.'], 422);
            }
            Session::flash('error', "Oops!! Category Failed To Save.");
            return Redirect::back();
        }
    }

    public function categoryUpdate(Request $request, JobCategory $jobCategory)
    {
        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }

        $input = $request->except('_token');
        $save_category = $jobCategory->update($input);
        if($save_category){
            if($request->ajax()){
                return response()->json(['msg' => 'Category Saved Successfully!!'], 200);
            }
            Session::flash('success', "Category Saved Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Category Failed To Save.'], 422);
            }
            Session::flash('error', "Oops!! Category Failed To Save.");
            return Redirect::back();
        }
    }


    public function categoryDestroy(Request $request, JobCategory $jobCategory)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $destroy = $jobCategory->delete();
        if($destroy){
            if($request->ajax()){
                return response()->json(['msg' => 'Category Deleted Successfully!!'], 200);
            }
            Session::flash('success', "Category Deleted Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Category Failed To Delete.'], 422);
            }
            Session::flash('error', "Oops!! Category Failed To Delete.");
            return Redirect::back();
        }
    }




    // Jobs Methods

    public function jobs()
    {
        $jobs = Job::latest()->paginate(20);
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        return view('admin.jobs.index', compact('jobs'));
    }


    public function jobAdd(Job $job)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }

        return view('admin.jobs.create', compact('job'));
    }
    public function jobSave(Request $request)
    {
        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }

        $this->validate($request, [
           'title' => 'string|required|min:3',
           'description' => 'string|required|min:3',
           'status' => 'required',
           'type_id' => 'required',
           'cat_id' => 'required',
           'wages' => 'required',
           'duration' => 'required',
           'close_date' => 'required|date',
        ]);

        $input = $request->all();
        $input['user_id'] = \Auth::id();
        $input['company_id'] = 1;
        $save_job = Job::create($input);
        if($save_job){
            if($request->ajax()){
                return response()->json(['msg' => 'Job Saved Successfully!!'], 200);
            }
            Session::flash('success', "Job Saved Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Job Failed To Save.'], 422);
            }
            Session::flash('error', "Oops!! Job Failed To Save.");
            return Redirect::back();
        }
    }
    public function jobUpdate(Request $request, Job $job)
    {
        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }

        $this->validate($request, [
            'title' => 'string|required|min:3',
            'description' => 'string|required|min:3',
            'status' => 'required',
            'type_id' => 'required',
            'cat_id' => 'required',
            'wages' => 'required',
            'duration' => 'required',
        ]);

        $input = $request->except('_token', 'method');
        $save_job = $job->update($input);
        if($save_job){
            if($request->ajax()){
                return response()->json(['msg' => 'Job Saved Successfully!!'], 200);
            }
            Session::flash('success', "Job Saved Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Job Failed To Save.'], 422);
            }
            Session::flash('error', "Oops!! Job Failed To Save.");
            return Redirect::back();
        }
    }

    public function jobDestroy(Request $request, Job $job)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $destroy = $job->delete();
        if($destroy){
            if($request->ajax()){
                return response()->json(['msg' => 'Job Deleted Successfully!!'], 200);
            }
            Session::flash('success', "Job Deleted Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Job Failed To Delete.'], 422);
            }
            Session::flash('error', "Oops!! Job Failed To Delete.");
            return Redirect::back();
        }
    }

    public function applications()
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $applications = JobApplication::latest()->paginate(20);
        return view('admin.jobs.applications', compact('applications'));
    }
    public function jobApplications(Job $job)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $applications = $job->application()->get();
        return view('admin.jobs.applications', compact('applications'));
    }

    public function jobAppDestroy(Request $request, JobApplication $jobApplication)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        if(\File::exists(public_path($jobApplication->resume))){
            \File::delete(public_path($jobApplication->resume));
        }
        $has_assigned = $jobApplication->user->jobassigned->where('job_id', $jobApplication->job_id)->first();
        if ($has_assigned) {
            $has_assigned->delete();
        }
        $destroy = $jobApplication->delete();
        if($destroy){
            if($request->ajax()){
                return response()->json(['msg' => 'Job Application Deleted Successfully!!'], 200);
            }
            Session::flash('success', "Job Application Deleted Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Job Application Failed To Delete.'], 422);
            }
            Session::flash('error', "Oops!! Job Application Failed To Delete.");
            return Redirect::back();
        }
    }
    public function jobAppReject(Request $request, JobApplication $jobApplication)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $reject = $jobApplication->update(['status' => '-1']);
        if($reject){
            $has_assigned = $jobApplication->user->jobassigned->where('job_id', $jobApplication->job_id)->first();
            if ($has_assigned) {
                if ($has_assigned->delete()) {
                    if($request->ajax()){
                        return response()->json(['msg' => 'Job Application Rejected Successfully!!'], 200);
                    }
                    Session::flash('success', "Job Application Rejected Successfully!!");
                    return Redirect::back();
                } else {
                    if($request->ajax()){
                        return response()->json(['msg' => 'Oops!! Job Application Rejection Failed.'], 422);
                    }
                    Session::flash('error', "Oops!! Job Application Rejection Failed.");
                    return Redirect::back();
                }
            }else{

                if($request->ajax()){
                    return response()->json(['msg' => 'Job Application Rejected Successfully!!'], 200);
                }
                Session::flash('success', "Job Application Rejected Successfully!!");
                return Redirect::back();
            }
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Job Application Rejection Failed.'], 422);
            }
            Session::flash('error', "Oops!! Job Application Rejection Failed.");
            return Redirect::back();
        }
    }

    public function jobAppAccept(Request $request, JobApplication $jobApplication)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $accept = $jobApplication->update(['status' => '2']);
        if ($accept) {
            $has_assigned = $jobApplication->user->jobassigned->where('job_id', $jobApplication->job_id)->first();
            if ($has_assigned) {
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Job has already been assigned to this User.'], 422);
                }
                Session::flash('error', "Oops!! Job has already been assigned to this User.");
                return Redirect::back();
            } else {
                $assigned = new JobAssigned();
                $assigned->user_id = $jobApplication->user_id;
                $assigned->job_id = $jobApplication->job_id;
                $assigned->status = '2';
                if($assigned->save()){
                    if($request->ajax()){
                        return response()->json(['msg' => 'Job Application Accepted Successfully!!'], 200);
                    }
                    Session::flash('success', "Job Application Accepted Successfully!!");
                    return Redirect::back();

                }else{
                    if($request->ajax()){
                        return response()->json(['msg' => 'Oops!! Job Application Acceptance Failed.'], 422);
                    }
                    Session::flash('error', "Oops!! Job Application Acceptance Failed.");
                    return Redirect::back();
                }
            }
        } else {
            # code...
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cPassword()
    {
        if (Gate::denies('admin')) {
            return view('users.cpassword');
        }
        return view('admin.cpassword');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required'],
            'new_password' => ['required','string','min:8'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        if (Hash::check($request->current_password, \Auth::user()->password)) {
            //add logic here
            $change = User::find(\Auth::id())->update(['password'=> Hash::make($request->new_password)]);
            if($change){
                if($request->ajax()){
                    return response()->json(['msg' => 'Password Changed Successfully!!'], 200);
                }
                Session::flash('success', "Password Changed Successfully!!");
                return Redirect::back();
            }else{
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Password Failed To Change.'], 422);
                }
                Session::flash('error', "Oops!! Password Failed To Change.");
                return Redirect::back();
            }
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Incorrect Password.'], 422);
            }
            Session::flash('error', "Oops!! Incorrect Password.");
            return Redirect::back();
        }


    }


    public function users()
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }


        if(isset($_GET['q']) && $_GET['q'] != ""){
            $search = $_GET['q'];
            $users = User::where('fname', 'LIKE', '%' . $search . '%')
            ->orWhere('lname', 'LIKE', '%' . $search . '%')
            ->orWhere('mname', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search . '%')
            ->paginate(20);
        }else{
            $users = User::paginate(20);
        }
        return view('admin.users.index', compact('users'));
    }

    public function userAdd(User $user)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }

        return view('admin.users.create', compact('user'));
    }


    public function userSave(Request $request)
    {

        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }
        $message = array(
            'fname.required' => 'Please enter Your First Name!!',
            'lname.required' => 'Please enter Your Last Name!!',
            'email.required' => 'Please enter a Valid Email Address!!',
            'tel.required' => 'Please enter a Valid Phone Number!!',
            'password.required' => 'Password is Mandatory!!',
        );
        $request->validate([
            'fname' => ['required', 'string', 'max:255', 'min:3'],
            'lname' => ['required', 'string', 'max:255', 'min:3'],
            'tel' => ['required', 'string', 'max:255', 'min:6', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], $message);
        $input = $request->all();
        $input['password'] = Hash::make($request->password);
        $save_user = User::create($input);
        if($save_user){
            if($request->ajax()){
                return response()->json(['msg' => 'User Saved Successfully!!'], 200);
            }
            Session::flash('success', "User Saved Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! User Failed To Save.'], 422);
            }
            Session::flash('error', "Oops!! User Failed To Save.");
            return Redirect::back();
        }
    }
    public function userUpdate(Request $request, User $user)
    {
        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }

        $message = array(
            'fname.required' => 'Please enter Your First Name!!',
            'lname.required' => 'Please enter Your Last Name!!',
            'email.required' => 'Please enter a Valid Email Address!!',
            'tel.required' => 'Please enter a Valid Phone Number!!',
        );
        $request->validate([
            'fname' => ['required', 'string', 'max:255', 'min:3'],
            'lname' => ['required', 'string', 'max:255', 'min:3'],
            'tel' => ['required', 'string', 'max:255', 'min:6'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ], $message);
        $input = $request->except('_token');
        $save_user = $user->update($input);
        if($save_user){
            if($request->ajax()){
                return response()->json(['msg' => 'User Saved Successfully!!'], 200);
            }
            Session::flash('success', "User Saved Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! User Failed To Save.'], 422);
            }
            Session::flash('error', "Oops!! User Failed To Save.");
            return Redirect::back();
        }
    }

    public function userDestroy(Request $request, User $user)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $destroy = $user->delete();
        if($destroy){
            if($request->ajax()){
                return response()->json(['msg' => 'User Deleted Successfully!!'], 200);
            }
            Session::flash('success', "User Deleted Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! User Failed To Delete.'], 422);
            }
            Session::flash('error', "Oops!! User Failed To Delete.");
            return Redirect::back();
        }
    }
    public function userDeactivate(Request $request, User $user)
    {
        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }

        $deactivate = $user->update(['status' => '0']);
        if($deactivate){
            if($request->ajax()){
                return response()->json(['msg' => 'User Deactivated Successfully!!'], 200);
            }
            Session::flash('success', "User Deactivated Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! User Failed To Deactivate.'], 422);
            }
            Session::flash('error', "Oops!! User Failed To Deactivate.");
            return Redirect::back();
        }
    }

    public function userActivate(Request $request, User $user)
    {
        if (Gate::denies('admin')) {
            if($request->ajax()){
                return response()->json(['msg' => 'Unauthorized Access!!'], 403);
            }
            Session::flash('error', "Unauthorized Access!!");
            return redirect()->route('dashboard.home');
        }

        $activate = $user->update(['status' => '1']);
        if($activate){
            if($request->ajax()){
                return response()->json(['msg' => 'User Activated Successfully!!'], 200);
            }
            Session::flash('success', "User Activated Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! User Failed To Activate.'], 422);
            }
            Session::flash('error', "Oops!! User Failed To Activate.");
            return Redirect::back();
        }
    }
    public function employees()
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        if(isset($_GET['q']) && $_GET['q'] != ""){
            $search = $_GET['q'];
            $employees = Employee::where('fname', 'LIKE', '%' . $search . '%')
            ->orWhere('lname', 'LIKE', '%' . $search . '%')
            ->orWhere('mname', 'LIKE', '%' . $search . '%')
            ->orWhere('email', 'LIKE', '%' . $search . '%')
            ->paginate(20);
        }else{
            $employees = Employee::paginate(20);
        }
        return view('admin.employees', compact('employees'));
    }
    public function employeeDestroy(Request $request, Employee $employee)
    {
        if (Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }
        $destroy = $employee->delete();
        if($destroy){
            if($request->ajax()){
                return response()->json(['msg' => 'Employee Deleted Successfully!!'], 200);
            }
            Session::flash('success', "Employee Deleted Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Employee Failed To Delete.'], 422);
            }
            Session::flash('error', "Oops!! Employee Failed To Delete.");
            return Redirect::back();
        }
    }

    //Users Methods Ends
    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function employee(Employee $employee)
    {
        return view('admin.employee', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
