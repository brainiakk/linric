<?php

namespace App\Http\Controllers;

use App\BankDetails;
use App\EmployeeDocuments;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Response;
use Gate;
use File;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = \Auth::user();
        return view('users.profile', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        $bank = \Auth::user()->bank != "" ? \Auth::user()->bank : new BankDetails();
        $doc = \Auth::user()->documents != "" ? \Auth::user()->documents : new EmployeeDocuments();
        return view('users.account', compact('bank', 'doc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bankDetails(Request $request)
    {
        $message = array(
            'bank_name.required' => 'Please enter Your Bank Name!!',
            'account_name.required' => 'Please enter Your Account Name!!',
            'account_number.required' => 'Please enter a Valid Account Number!!',
            'routing_number.required' => 'Please enter Your Bank\'s Routing Number!!',
        );
        $request->validate([
            'account_name' => ['required', 'string', 'max:255', 'min:3'],
            'bank_name' => ['required', 'string', 'max:255', 'min:3'],
            'routing_number' => ['required', 'numeric'],
            'account_number' => ['required', 'numeric'],
        ], $message);
        if (\Auth::user()->bank()->exists()) {
            $input = $request->except('_token');
            $input['user_id'] = \Auth::id();
            $bank_save = \Auth::user()->bank()->update($input);
            if ($bank_save) {
                if($request->ajax()){
                    return response()->json(['msg' => 'Bank Details Updated Successfully!!'], 200);
                }
                Session::flash('success', "Bank Details Updated Successfully!!");
                return Redirect::back();
            }else{

            }
        }else{
            $input = $request->all();
            $input['user_id'] = \Auth::id();
            $bank_save = BankDetails::create($input);
            if ($bank_save) {
                if($request->ajax()){
                    return response()->json(['msg' => 'Bank Details Saved Successfully!!'], 200);
                }
                Session::flash('success', "Bank Details Saved Successfully!!");
                return Redirect::back();
            }else{

            }
        }
    }
    public function documentUpload(Request $request)
    {
        $message = array(
            'ssn.max' => ' SSN Card File Too Large!! (The document may not be greater than 5 megabytes)',
            'resume.max' => ' Resume File Too Larget!! (The document may not be greater than 5 megabytes)',
            'passport.max' => ' Passport File Too Large!! (The document may not be greater than 5 megabytes)',
            'id.max' => ' Valid ID Card File Too Large!! (The document may not be greater than 5 megabytes)',
            'resume.required' => 'Please upload your resume!!',
            'ssn.required' => 'Please upload Your SSN Card!!',
            // 'id.required' => 'Please upload a valid ID Card!!',
            // 'passport.required' => 'Please upload a legible Passport!!',
            'ssn.mimes' => 'Invalid SSN Card File Format!!',
            'resume.mimes' => 'Invalid Resume File Format!!',
            'passport.mimes' => 'Invalid Passport File Format!!',
            'id.mimes' => 'Invalid Valid ID Card File Format!!',
        );
        $request->validate([
            'resume' => ['required', 'max:8192', 'mimes:doc,pdf,docx,jpg,jpeg,png'],
            'ssn' => ['required', 'max:5084', 'mimes:doc,pdf,docx,jpg,jpeg,png'],
            'id' => [ 'max:5084', 'mimes:doc,pdf,docx,jpg,jpeg,png'],
            'passport' => [ 'max:5084', 'mimes:bmp,pdf,docx,jpg,jpeg,png'],
        ], $message);
        $documents = ['resume', 'ssn', 'passport', 'id'];


        foreach ($documents as $document) {

            if ($request->hasfile($document)) {
                $file = $request->file($document);
                $name = \Str::slug(\Auth::user()->name()).'-'.$document.'-'.time().'.'.$file->getClientOriginalExtension();
                $file->move(public_path().'/uploads/employee_documents/'.$document.'/', $name);
                $user_doc =  EmployeeDocuments::whereUser_id(\Auth::id())->whereType($document)->first();
                if(!$user_doc){
                    $new = EmployeeDocuments::create([
                        'user_id' => \Auth::id(),
                        'filename' => $name,
                        'type' => $document
                    ]);
                    if (!$new) {
                        if($request->ajax()){
                            return response()->json(['msg' => 'Oops!! '.$document.' Failed to Upload'], 422);
                        }
                        Session::flash('error', "Oops!! ".$document.' Failed to Upload');
                        return Redirect::back();
                    }
                }else{
                    $update =  EmployeeDocuments::whereUser_id(\Auth::id())->whereType($document)->update(['filename' => $name], $request->except('_token'));
                    if (!$update) {
                        if($request->ajax()){
                            return response()->json(['msg' => 'Oops!! '.$document.' Failed to Upload'], 422);
                        }
                        Session::flash('error', "Oops!! ".$document.' Failed to Upload');
                        return Redirect::back();
                    }
                }
            }
        }
        if($request->ajax()){
            return response()->json(['msg' => 'Documents Uploaded Successfully!!'], 200);
        }
        Session::flash('success', "Documents Uploaded Successfully!!");
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function profileUpdate(User $user, Request $request)
    {
        $message = array(
            'fname.required' => 'Please enter Your First Name!!',
            'lname.required' => 'Please enter Your Last Name!!',
            'email.required' => 'Please enter a Valid Email Address!!',
            'tel.required' => 'Please enter a Valid Phone Number!!',
        );
        $request->validate([
            'fname' => ['required', 'string', 'max:255', 'min:3'],
            'lname' => ['required', 'string', 'max:255', 'min:3'],
            'tel' => ['required', 'string', 'max:255', 'min:6'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ], $message);
        $input = $request->except('_token');
        if ($request->email == $user->email) {
            $input['email'] = $request->email;
        }else{
            $verifyy = User::whereEmail($request->email)->where('email', '!=', $user->email)->first();
            if ($verifyy) {
                Session::flash('error', "Email address already in use by another user.");
                return Redirect::back();
            } else {
                $input['email'] = $request->email;
            }
        }
        $save_user = auth()->user()->update($input);
        if($save_user){
            if($request->ajax()){
                return response()->json(['msg' => 'Profile Updated Successfully!!'], 200);
            }
            Session::flash('success', "Profile Updated Successfully!!");
            return Redirect::back();
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Profile Failed To Update.'], 422);
            }
            Session::flash('error', "Oops!! Profile Failed To Update.");
            return Redirect::back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function deactivate(User $user, Request $request)
    {
        if (\Auth::user()->isOwner($user->user_id)) {
            $deactivate = \Auth::user()->update(['status' => '0']);
            if($deactivate){
                return redirect()->route('logout');
            }else{
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Something went wrong.'], 422);
                }
                Session::flash('error', "Oops!! Something went wrong.");
                return Redirect::back();
            }
        }else{
            if($request->ajax()){
                return response()->json(['msg' => 'Oops!! Something went wrong.'], 422);
            }
            Session::flash('error', "Oops!! Something went wrong.");
            return Redirect::back();
        }
    }
    public function destroy(User $user)
    {
        //
    }
}
