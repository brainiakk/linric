<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Gate::denies('admin')) {
            return redirect()->route('dashboard.home');
        }

        $company = Company::first();
        if ($company)
        {
            $company = $company;
        }
        else
        {
            $company = new Company();
        }
        return view('admin.company', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|regex:/^[a-zA-Z1-9\s]*$/',
            'email' => 'required|email',
            'tel' => 'required',
            'address' => 'required|string',
        ],
            [
                'name.required'  => 'Company Name Field is Compulsory!!',
                // 'subject.regex'  => 'We do not allow suspicious characters in our inquiry box, please use plain English, Thanks.',
                'email.required'  => 'Please provide a valid mail, so your users can reach you!!',
                'email.email'  => 'Please provide a valid mail, so your users can reach you!!',
                'tel.required'  => 'Please provide a valid telephone, so your users can reach you!!',
                'address.required'  => 'Office Address Field is Compulsory!!',
                // 'msg.regex'  => 'We do not allow suspicious characters in our inquiry box, please use plain English, Thanks.',
            ]);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(Company::find(1)){
            $input = $request->except('_token');
            $company_save = Company::find(1)->update($input);
            if($company_save){
                if($request->ajax()){
                    return response()->json(['msg' => 'Company Data Updated Successfully!!'], 200);
                }
                Session::flash('success', "Company Data Updated Successfully!!");
                return Redirect::back();
            }else{
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Company Data Failed To Save.'], 422);
                }
                Session::flash('error', "Oops!! Company Data Failed To Save.");
                return Redirect::back();
            }
        }else{
            $input = $request->all();
            $input['user_id'] = auth()->id();
            $company_save = Company::create($input);
            if($company_save){
                if($request->ajax()){
                    return response()->json(['msg' => 'Company Data Saved Successfully!!'], 200);
                }
                Session::flash('success', "Company Data Saved Successfully!!");
                return Redirect::back();
            }else{
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Company Data Failed To Save.'], 422);
                }
                Session::flash('error', "Oops!! Company Data Failed To Save.");
                return Redirect::back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */

    public function uploadImage(Request $request)
    {
        if ($request->hasfile('image')) {
            $image = $request->file('image');
            $filename = 'logo.' . $image->getClientOriginalExtension();
            $location = public_path('images/resource/') . $filename;

            if(Image::make($image)->save($location)){
                return response()->json(['message' => 'Uploaded', 'image' => asset('images/resource/'.$filename), 'imageName' => $filename], 200);
            }
            return response()->json(['message' => 'Failed'], 422);
        }
    }
    public function ajaxImageUploadPost(Request $request)

    {

        $validator = Validator::make($request->all(), [

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);


        if ($validator->passes()) {


            $input = $request->all();

            $input['image'] = time().'.'.$request->image->extension();

            $request->image->move(public_path('images'), $input['image']);

            $company_save = Company::find(1)->update(['image' => $input], except('_token'));
            if($company_save){
                if($request->ajax()){
                    return response()->json(['msg' => 'Company Data Saved Successfully!!'], 200);
                }
                Session::flash('success', "Company Data Saved Successfully!!");
                return Redirect::back();
            }else{
                if($request->ajax()){
                    return response()->json(['msg' => 'Oops!! Company Data Failed To Save.'], 422);
                }
                Session::flash('error', "Oops!! Company Data Failed To Save.");
                return Redirect::back();
            }


            return response()->json(['success'=>'done']);

        }


        return response()->json(['error'=>$validator->errors()->all()]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
