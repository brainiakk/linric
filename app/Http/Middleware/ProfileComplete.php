<?php

namespace App\Http\Middleware;

use Closure;

class ProfileComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user()->isAdmin()) {
            # code...
            return $next($request);
        } else {
            if ($request->user()->employee()->exists()) {
                return $next($request);
            }
            return redirect()->route('dashboard.become.employee');
        }
    }
}
