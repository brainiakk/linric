<?php

namespace App\Providers;

use App\Models\Company;
use App\Models\Visitor;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Paginator::useBootstrap();
        //
        Schema::defaultStringLength(191);
        if($company = Company::first()){
            $tel = $company->tel;
            $address = $company->address;
            $email = $company->email;
            $company_name = $company->name;
        }else{
            $address = '16 Edgerton Terrace East Orange New Jersey 07017';
            $tel = '+1973-609-8466';
            $email = 'info@linricproservices.com';
            $company_name = 'Linric Professional Services';
        }
        View::share('appName', $company_name);
        View::share('appCr', 'Think Free Developers');
        View::share('appCl', 'https://thinkfreedeveloper.com');
        View::share('appAddress', $address);
        View::share('appTel', $tel);
        View::share('appFax', '973-609-8466');
        View::share('appEmail', $email);
        View::share('appEmail2', 'e.okoemu@linricproservices.com');


        View::share('yr', date("Y"));
        View::share('mnth', date("M"));
        View::share('mth', date("m"));
        View::share('day', date("D"));

        //Daily Visitors Counter
        $dates = date("Y-m-d");
        $userIP = \Request::ip();

        $chk_v = Visitor::where('date', $dates)->first();

        if (!$chk_v){
            $visitor = new Visitor();
            $visitor['ip'] = $userIP;
            $visitor['date'] = $dates;
            $visitor['views'] = 1;
            $visitor->save();

        }else {
            if (!preg_match('/' . $userIP . '/i', $chk_v->ip)) {
                $newIP = $chk_v->ip.''.$userIP;
                Visitor::where('date', $dates)->update(['ip'=> $newIP, 'views'=>$chk_v->views + 1]);

            }
        }
        //End Daily Visitors Counter
    }
}
