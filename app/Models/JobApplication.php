<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'job_id', 'company_id', 'resume', 'cover_letter', 'status'];

    public function scopeOwner($scope)
    {
        return $scope->where('user_id', auth()->id());
    }
    public function scopeActive($scope)
    {
        return $scope->where('status', '1');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function job()
    {
        return $this->belongsTo(Job::class, 'job_id');
    }

}
