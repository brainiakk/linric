<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'fname', 'mname', 'lname', 'email', 'password', 'tel', 'hear',  'gender', 'password', 'status', 'level',
    ];




    public function roles(){
        return $this->belongsToMany(Role::class, 'role_user', 'user_id');
    }
    public function employee(){
        return $this->hasOne('App\Models\Employee', 'user_id');
    }
    public function jobassigned(){
        return $this->hasMany('App\Models\JobAssigned', 'user_id');
    }
    public function application()
    {
        return $this->hasMany('App\Models\JobApplication', 'user_id');
    }

    public function job(){
        return $this->hasMany('App\Models\Job', 'user_id');
    }

    public function leave(){
        return $this->hasMany('App\Models\Leave', 'user_id');
    }
    public function education(){
        return $this->hasMany('App\Models\EmployeeEducation', 'user_id');
    }
    public function work_history(){
        return $this->hasMany('App\Models\EmployeeWorkHistory', 'user_id');
    }
    public function reference(){
        return $this->hasMany('App\Models\EmployeeReference', 'user_id');
    }

    public function bank(){
        return $this->hasOne('App\Models\BankDetails', 'user_id');
    }

    public function documents(){
        return $this->hasOne('App\Models\EmployeeDocuments', 'user_id');
    }

    public function company(){
        return $this->hasOne('App\Models\Company', 'user_id');
    }

    public function name()
    {
        return $this->fname.' '.$this->mname.' '.$this->lname;
    }

    public function isOwner($owner)
    {
        if ($this->user_id == $owner) {
            return true;
        }else{
            return false;
        }
    }

    public function avatar(){
        if ($this->gender == 'm') {
            return asset('/avatars/male.png');
        } else {
            return asset('/avatars/female.png');
        }
    }
    public static function Role()
    {
        # code...
        if(\Auth::user()){
            $role = \Auth::user()->user_level;
            return $role;
        }else{
            return false;
        }
    }
    public function isAdmin()
    {
        # code...
        if(\Auth::check()){
            if (\Auth::user()->level == 1) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }
    public function isEmployee()
    {
        # code...
        if(\Auth::check()){
            if ($this->isAdmin()) {
                return true;
            }else{
                if (\Auth::user()->level == 0 && \Auth::user()->employee()->exists()) {
                    return true;
                } else {
                    return false;
                }
            }
        }else{
            return false;
        }
    }
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isSuper()
    {
        if ($this->level > 1){
            return true;
        }
        return false;
    }
}
