<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
    use HasFactory;

    protected $primaryKey = 'type_id';

    protected $fillable = ['type_name', 'status'];

    public function scopeActive($scope)
    {
        return $scope->whereStatus('1');
    }

    public function jobs()
    {
        return $this->hasMany(Job::class, 'type_id');
    }
}
