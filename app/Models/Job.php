<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Job extends Model
{
    use HasFactory;
    protected $fillable = ['company_id','user_id','cat_id','type_id','title','description','close_date',
        'status','city','location','duration','wages','experience',];

    protected $casts = [
        'close_date' => 'datetime:Y-m-d'
    ];



    public function scopeActive($scope)
    {
        return $scope->whereStatus('1');
    }

    public function scopeAvailable($scope)
    {
        return $scope->whereStatus('1')->where('close_date', '>=', Carbon::now()->format('Y-m-d'));
    }


    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    public function jobassigned()
    {
        return $this->hasOne(JobAssigned::class, 'job_id');
    }
    public function application()
    {
        return $this->hasMany(JobApplication::class, 'job_id');
    }
    public function category()
    {
        return $this->belongsTo(JobCategory::class, 'cat_id');
    }
    public function type()
    {
        return $this->belongsTo(JobType::class, 'type_id');
    }

    public function intro($max_len = 150)
    {
        $base_text_to_use = $this->description;
        if (!trim($base_text_to_use)) {
            $base_text_to_use = $this->description;
        }
        $base_text_to_use2 = strip_tags($base_text_to_use);

        $intro = Str::limit($base_text_to_use2, (int)$max_len);
        return $intro;
    }
}
