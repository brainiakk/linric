<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    use HasFactory;

    protected $primaryKey = 'cat_id';

    protected $fillable = ['cat_name', 'status'];

    public function scopeActive($scope)
    {
        return $scope->whereStatus('1');
    }


    public function job()
    {
        return $this->hasMany(Job::class, 'cat_id')->whereStatus('1');
    }
}
