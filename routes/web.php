<?php

use App\Models\Job;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $jobs = Job::active()->orderBy('updated_at','DESC');
    return view('pages.index', compact('jobs'));
})->name('home');

Route::get('/about', function () {
    return view('pages.about');
})->name('about');

Route::get('/contact', function () {
    return view('pages.contact');
})->name('contact');

Route::post('contact-send','App\Http\Controllers\ContactController@sendMail')->name('contact.send');


Auth::routes();

Route::prefix('jobs')->name('jobs.')->group( function () {
    Route::get('/','App\Http\Controllers\JobController@index')->name('home');
    Route::get('/{job}','App\Http\Controllers\JobController@show')->name('show');
    Route::post('/','App\Http\Controllers\JobController@searchJobs')->name('search');
    Route::get('/applied','App\Http\Controllers\JobController@applied')->name('applied');
});

Route::get('/home','App\Http\Controllers\HomeController@index');
Route::prefix('dashboard')->middleware('auth')->name('dashboard.')->group(function(){
    Route::get('/profile/{user}','App\Http\Controllers\AdminController@userProfile')->name('profile.user');
    Route::get('/profile','App\Http\Controllers\AccountController@index')->name('profile.user');
    Route::patch('/profile/update/{user}','App\Http\Controllers\AccountController@profileUpdate')->name('profile.update');
    Route::get('/become/employee','App\Http\Controllers\EmployeeController@create')->name('become.employee');
    Route::post('/become/employee','App\Http\Controllers\EmployeeController@store')->name('become.employee');
    Route::get('/change-password','App\Http\Controllers\AdminController@cPassword')->name('change.password');
    Route::get('/deactivate/{user}','App\Http\Controllers\AccountController@deactivate')->name('deactivate.account');
    //User Leaves Route
    Route::get('/leaves','App\Http\Controllers\LeaveController@index')->name('leaves.applied');
    Route::get('/leave/delete/{leave}','App\Http\Controllers\LeaveController@destroy')->name('leave.delete');
    Route::post('/leave/apply','App\Http\Controllers\LeaveController@store')->name('leave.apply');

    Route::post('/password/change','App\Http\Controllers\AdminController@changePassword')->name('password.change');
    Route::get('/','App\Http\Controllers\AdminController@index')->name('home');

        Route::get('/jobs/applied','App\Http\Controllers\JobController@applied')->name('jobs.applied');
        Route::get('/jobs','App\Http\Controllers\JobAssignedController@index')->name('jobs.assigned');
        Route::get('/jobs/assigned/delete/{jobAssigned}','App\Http\Controllers\JobAssignedController@destroy')->name('jobs.assigned.delete');
        Route::get('/jobs/applied/delete/{jobApplication}','App\Http\Controllers\JobController@jobAppDestroy')->name('jobs.applied.delete');
        Route::post('/jobs/apply/{job}','App\Http\Controllers\JobController@jobApply')->name('jobs.apply');
        Route::get('/account/settings','App\Http\Controllers\AccountController@settings')->name('account.settings');
        Route::post('/bank/store','App\Http\Controllers\AccountController@bankDetails')->name('bank.store');
        Route::post('/document/store','App\Http\Controllers\AccountController@documentUpload')->name('document.store');

    Route::prefix('admin')->name('admin.')->middleware('can:admin')->group(function(){
        Route::get('/','App\Http\Controllers\AdminController@adminHome')->name('home');

        // Admin Leave Routes
        Route::get('/leaves','App\Http\Controllers\LeaveController@adminLeaves')->name('leaves');
        Route::get('/leave/reject/{leave}','App\Http\Controllers\LeaveController@rejectLeave')->name('leave.reject');
        Route::get('/leave/accept/{leave}','App\Http\Controllers\LeaveController@acceptLeave')->name('leave.accept');

        Route::get('/company','App\Http\Controllers\CompanyController@index')->name('company');
        Route::post('/company/save','App\Http\Controllers\CompanyController@store')->name('company.save');
        Route::post('/company/logo/upload','App\Http\Controllers\CompanyController@uploadImage')->name('company.logo.upload');
        Route::prefix('employees')->name('employees.')->group(function(){
            Route::get('/','App\Http\Controllers\AdminController@employees')->name('home');
            Route::get('/delete/{employee}','App\Http\Controllers\AdminController@employeeDestroy')->name('delete');
            Route::get('/{employee}','App\Http\Controllers\AdminController@employee')->name('show');
        });
        Route::prefix('users')->name('users.')->group(function(){
            Route::get('/','App\Http\Controllers\AdminController@users')->name('home');
            Route::post('/store','App\Http\Controllers\AdminController@userSave')->name('store');
            Route::get('/add','App\Http\Controllers\AdminController@userAdd')->name('add');
            Route::patch('/update/{user}','App\Http\Controllers\AdminController@userUpdate')->name('update');
            Route::get('/edit/{user}','App\Http\Controllers\AdminController@userAdd')->name('edit');
            Route::get('/delete/{user}','App\Http\Controllers\AdminController@userDestroy')->name('delete');
            Route::get('/activate/{user}','App\Http\Controllers\AdminController@userActivate')->name('activate');
            Route::get('/deactivate/{user}','App\Http\Controllers\AdminController@userDeactivate')->name('deactivate');
        });
        Route::prefix('category')->name('category.')->group(function(){
            Route::get('/','App\Http\Controllers\AdminController@category')->name('home');
            Route::post('/store','App\Http\Controllers\AdminController@categorySave')->name('store');
            Route::get('/add','App\Http\Controllers\AdminController@categoryAdd')->name('add');
            Route::patch('/update/{jobCategory}','App\Http\Controllers\AdminController@categoryUpdate')->name('update');
            Route::get('/edit/{jobCategory}','App\Http\Controllers\AdminController@categoryAdd')->name('edit');
            Route::get('/delete/{jobCategory}','App\Http\Controllers\AdminController@categoryDestroy')->name('delete');
        });
        Route::prefix('jobs')->name('jobs.')->group(function(){
            Route::get('/','App\Http\Controllers\AdminController@jobs')->name('home');
            Route::get('/add','App\Http\Controllers\AdminController@jobAdd')->name('add');
            Route::post('/store','App\Http\Controllers\AdminController@jobSave')->name('store');
            Route::patch('/update/{job}','App\Http\Controllers\AdminController@jobUpdate')->name('update');
            Route::get('/edit/{job}','App\Http\Controllers\AdminController@jobAdd')->name('edit');
            Route::get('/delete/{job}','App\Http\Controllers\AdminController@jobDestroy')->name('delete');
            Route::get('/applications','App\Http\Controllers\AdminController@applications')->name('applications');
            Route::get('/{job}/applications','App\Http\Controllers\AdminController@jobApplications')->name('applications.specific');
            Route::get('/applications/delete/{jobApplication}','App\Http\Controllers\AdminController@jobAppDestroy')->name('applications.delete');
            Route::get('/applications/accept/{jobApplication}','App\Http\Controllers\AdminController@jobAppAccept')->name('applications.accept');
            Route::get('/applications/reject/{jobApplication}','App\Http\Controllers\AdminController@jobAppReject')->name('applications.reject');
            Route::get('/candidates','App\Http\Controllers\AdminController@jobCandidates')->name('candidates');
        });
    });
});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
