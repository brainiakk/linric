@extends('layouts.master')
@section('title', 'Job Board')
@section('content')
@include('inc.breadcrumb')
<section class="find-job job-list section">
    <div class="container">
        <div class="single-head">
            <div class="row">
                @include('jobs.load')
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="pagination center">
                        <ul class="pagination-list">
                            {!! $jobs->links() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
