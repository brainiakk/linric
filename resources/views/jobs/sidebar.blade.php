<!-- Widgets -->
<div class="five columns">

    <form method="POST" action="{{ route('jobs.search') }}">
    @csrf
    <!-- Keyword -->
        <div class="widget">
            <h4>Keyword</h4>
            <input type="text" name="search" placeholder="job title or keywords" value=""/>

        </div>

        <!-- Sort by -->
        <div class="widget">
            <h4>Sort by</h4>
            <!-- Select -->
            <select data-placeholder="Choose Category" name="sort" class="chosen-select-no-single">
                <option selected="selected" value="recent">Newest</option>
                <option value="oldest">Oldest</option>
                <option value="ratehigh">Hourly Rate – Highest First</option>
                <option value="ratelow">Hourly Rate – Lowest First</option>
            </select>

        </div>

        <!-- Job Type -->
        <div class="widget">
            <h4>Job Type</h4>

            <ul class="checkboxes">
                <li>
                    <input id="check-1" type="checkbox" name="check" value="check-1" checked>
                    <label for="check-1">Any Type</label>
                </li>
                @forelse(\App\Models\JobType::active()->limit('6')->get() as $type)
                    <li>
                        <input id="job-type-{{ $type->type_id }}" type="checkbox" name="type_id" value="{{ $type->type_id }}">
                        <label for="job-type-{{ $type->type_id }}"> {{ $type->type_name }} <span>({{ count($type->jobs) }})</span></label>
                    </li>
                @empty
                @endforelse

            </ul>

        </div>

        <!-- Rate/Hr -->
        <div class="widget">
            <h4>Rate / Hr</h4>

            <ul class="radios">
                <li>
                    <input id="check-6" type="radio" name="rate" value="" checked>
                    <label for="check-6">Any Rate</label>
                </li>
                <li>
                    <input id="check-7" type="radio" name="rate" value="[0, 25]">
                    <label for="check-7">$0 - $25 <span>({{ count($jobs->whereBetween('wages', [0, 25])) }})</span></label>
                </li>
                <li>
                    <input id="check-8" type="radio" name="rate" value="[26, 50]">
                    <label for="check-8">$26 - $50 <span>({{ count($jobs->whereBetween('wages', [26, 50])) }})</span></label>
                </li>
                <li>
                    <input id="check-9" type="radio" name="rate" value="[51, 100]">
                    <label for="check-9">$51 - $100 <span>({{ count($jobs->whereBetween('wages', [51, 100])) }})</span></label>
                </li>
                <li>
                    <input id="check-10" type="radio" name="rate" value="[101, 200]">
                    <label for="check-10">$101 - $200 <span>({{ count($jobs->whereBetween('wages', [101, 200])) }})</span></label>
                </li>
                <li>
                    <input id="check-11" type="radio" name="rate" value="200">
                    <label for="check-11">$200+ <span>({{ count($jobs->where('wages', '>=', 200)) }})</span></label>
                </li>
            </ul>

        </div>
        <button style="width: 100%;" class="button border fw">Filter <i style="margin-left: 5px;" class="fa fa-arrow-right"></i></button>

    </form>

</div>
<!-- Widgets / End -->
