

<!-- Modal -->
<div class="modal fade" id="jobApply{{ $job->id }}" tabindex="2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Apply For {{ $job->title }}</h5>
            <button type="button" class="
            circle-32
            btn-reset
            bg-white
            pos-abs-tr
            mt-md-n6
            mr-lg-n6
            focus-reset
            z-index-supper
          " data-dismiss="modal">
                <i class="lni lni-close"></i>
            </button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <h5>Job Details</h5>
                  {!! $job->description !!}
                <form method="POST" action="{{ route('dashboard.jobs.apply', $job->id) }}" enctype="multipart/form-data" id="apply-form{{ $job->id }}">
                        @csrf
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Job Description*</label>
                            <textarea class="form-control" name="cover_letter" cols="40" rows="5" required></textarea>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <h5>Upload Resume <span>(max size: 4MB| Format: .pdf, .doc, .docx, .jpg, .png)</span></h5>
                            <label class="upload-button button">
                                <input id="cover_img_file_2" type="file"  name="resume" accept=".pdf, .doc, .docx, .jpg, .png">
                                <i class="fa fa-upload"></i> Browse
                            </label>
                            <span class="fake-input">No file selected</span>
                        </div>
                    </div>
                </form>
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" onclick="event.preventDefault(); document.getElementById('apply-form{{ $job->id }}').submit();" class="btn btn-success">Apply Now <i class="bx bx-right-arrow-alt" style="vertical-align: middle;"></i></button>
        </div>
      </div>
    </div>
  </div>

