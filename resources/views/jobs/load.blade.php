@forelse($jobs as $job)
    <div class="col-lg-6 col-12">
        <div class="single-job wow fadeInUp" data-wow-delay=".3s">
            <div class="job-image">

            </div>
            <div class="job-content">
                <h4><a href="{{ route('jobs.show', $job->id) }}">{{ $job->title }}</a></h4>
                <p>
                    {{ $job->intro() }}
                </p>
                <ul>
                    <li>
                        {{ $job->category->cat_name }}
                    </li>
                    <li><i class="lni lni-dollar"></i> {{ $job->wages }}/{{ $job->duration }}</li>
                    <li><i class="lni lni-map-marker"></i> {{ Str::limit($job->location, 15) }}</li>
                </ul>
            </div>
            <div class="job-button">
                <ul>
                    <li><a href="#jobApply{{ $job->id }}" data-toggle="modal" data-target="#jobApply{{ $job->id }}">Apply</a></li>
                    <li><span>{{ $job->type->type_name }}</span></li>
                </ul>
            </div>
        </div>
    </div>
    @push('apply-modal')
        @include('jobs.apply-modal')
    @endpush
@empty
<div class="alert alert-warning alert-block">
    <h4>Oops!! No Jobs Found at this time, check back later.  </h4>
</div>
@endforelse

