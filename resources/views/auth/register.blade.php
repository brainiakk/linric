@extends('layouts.master')
@section('title', 'Sign Up')
@section('content')
    <div class="breadcrumbs overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumbs-content">
                        <h1 class="page-title">@yield('title')</h1>
                        <p>
                        </p>
                    </div>
                    <ul class="breadcrumb-nav">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li>@yield('title')</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <section class="job-post section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 col-12">
                    <div class="job-information">
                        <h3 class="title">Create a new account</h3>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="form-group">
                                <label for="fname" class="label">First Name</label>
                                <input type="text" name="fname" id="fname" class="form-control @error('fname') is-invalid @enderror"  value="{{ old('fname') }}" required autocomplete="first-name" autofocus/>

                                @error('fname')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="mname" class="label">Middle Name</label>
                                <input type="text" name="mname" id="mname" class="form-control @error('mname') is-invalid @enderror"  value="{{ old('mname') }}"  autocomplete="middle-name" />

                                @error('mname')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="lname" class="label">Last Name</label>
                                <input type="text" name="lname" id="lname" class="form-control @error('lname') is-invalid @enderror"  value="{{ old('lname') }}" required autocomplete="last-name" />

                                @error('lname')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email" class="label">Email Address</label>
                                <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror"  value="{{ old('email') }}" required autocomplete="email"/>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password" class="label">Password</label>
                                <div class="position-relative">
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password"  required autocomplete="new-password" />
                                </div>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password" class="label">Repeat Password</label>
                                <div class="position-relative">
                                    <input type="password" name="password_confirmation" class="form-control @error('password') is-invalid @enderror"  id="password-confirm"  required autocomplete="new-password" />
                                </div>

                            </div>

                            <div class="form-group">
                                <label>Gender*</label>
                                <select name="gender" class="select">
                                    <option value=""></option>
                                    <option value="f">Female</option>
                                    <option value="m">Male</option>
                                </select>

                                @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="tel" class="label">Telephone</label>
                                <div class="position-relative">
                                    <input type="tel" name="tel" class="form-control @error('tel') is-invalid @enderror" id="tel"  required autocomplete="tel" />
                                </div>

                                @error('tel')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>

                            <div class="d-flex flex-wrap justify-content-between mb-5">
                                <div class="form-check">
                                    <input class="form-check-input" name="terms" type="checkbox"  id="flexCheckDefault" />
                                    <label class="form-check-label" for="flexCheckDefault">Agree to the <a href="#">Terms & Conditions</a></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 mb-8 button">
                                    <button type="submit" class="btn w-100">Sign Up</button>
                                </div>
                            </div>
                            <p class="text-center create-new-account mt-5">
                                Already have an account? <a href="{{ route('login') }}">Login</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
