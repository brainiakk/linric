@extends('layouts.master')
@section('title', 'Sign In')
@section('content')
<div class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h1 class="page-title">@yield('title')</h1>
                    <p>
                    </p>
                </div>
                <ul class="breadcrumb-nav">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>@yield('title')</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="job-post section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-12">
                <div class="job-information">
                    <h3 class="title">Sign in to continue your account and explore new jobs.</h3>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="label">E-mail</label>
                            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@gmail.com" id="email" />

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password" class="label">Password</label>
                            <div class="position-relative">
                                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Enter password" />
                            </div>

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                            @enderror
                        </div>
                        <div class="d-flex flex-wrap justify-content-between mb-5">
                        <div class="form-check">
                            <input class="form-check-input" name="remember" type="checkbox"  id="flexCheckDefault" />
                            <label class="form-check-label" for="flexCheckDefault">Remember password</label>
                        </div>
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}" class="font-size-3 text-dodger line-height-reset">Forget Password</a>
                        @endif
                        </div>
                        <div class="row">
                            <div class="col-lg-12 mb-8 button">
                                <button type="submit" class="btn w-100">Log in</button>
                            </div>
                        </div>
                        <p class="text-center create-new-account mt-5">
                            Don’t have an account? <a href="{{ route('register') }}">Create a free account</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
