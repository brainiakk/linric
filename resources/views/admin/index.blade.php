@extends('layouts.master')
@section('title', 'Admin Dashboard')
@section('content')
<!-- Dashboard -->
<div id="dashboard">

    <!-- Navigation
    ================================================== -->
    @include('inc.dashboard.sidebar')
    <!-- Content
    ================================================== -->
    <div class="dashboard-content">

        <!-- Titlebar -->
        <div id="titlebar">
            <div class="row">
                <div class="col-md-12">
                    <h2>Howdy, {{ auth()->user()->fname }}!</h2>
                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li>Dashboard</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>


        <!-- Content -->
        <div class="row">

            <!-- Item -->
            <div class="col-lg-3 col-md-6">
                <div class="dashboard-stat color-1">
                    <div class="dashboard-stat-content"><h4 class="counter">{{ $num_active_jobs }}</h4> <span>Active Job Listings</span></div>
                    <div class="dashboard-stat-icon"><i class="ln ln-icon-Folder"></i></div>
                </div>
            </div>

            <!-- Item -->
            <div class="col-lg-3 col-md-6">
                <div class="dashboard-stat color-2">
                    <div class="dashboard-stat-content"><h4 class="counter">{{ $num_jobs }}</h4> <span>Total Jobs Posted</span></div>
                    <div class="dashboard-stat-icon"><i class="ln ln-icon-Worker"></i></div>
                </div>
            </div>


            <!-- Item -->
            <div class="col-lg-3 col-md-6">
                <div class="dashboard-stat color-3">
                    <div class="dashboard-stat-content"><h4 class="counter">{{ $num_apps }}</h4> <span>Total Applications</span></div>
                    <div class="dashboard-stat-icon"><i class="ln ln-icon-Folder-WithDocument"></i></div>
                </div>
            </div>


            <!-- Item -->
            <div class="col-lg-3 col-md-6">
                <div class="dashboard-stat color-4">
                    <div class="dashboard-stat-content"><h4 class="counter">{{ $num_users }}</h4> <span>Total Users</span></div>
                    <div class="dashboard-stat-icon"><i class="ln ln-icon-Business-ManWoman"></i></div>
                </div>
            </div>


            <!-- Item -->
            <div class="col-lg-3 col-md-6">
                <div class="dashboard-stat color-2">
                    <div class="dashboard-stat-content"><h4 class="counter">{{ $num_visitors }}</h4> <span>Daily Visitors</span></div>
                    <div class="dashboard-stat-icon"><i class="ln ln-icon-People-onCloud "></i></div>
                </div>
            </div>

        </div>



    </div>
    <!-- Content / End -->


</div>
<!-- Dashboard / End -->
@endsection
