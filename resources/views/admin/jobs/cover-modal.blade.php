

<!-- Modal -->
<div class="modal fade" id="applicaionCL{{ $app->id }}" tabindex="2" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">{{ $app->job->title }} Application</h5>
            <button type="button" class="
            circle-32
            btn-reset
            bg-white
            pos-abs-tr
            mt-md-n6
            mr-lg-n6
            focus-reset
            z-index-supper
          " data-dismiss="modal">
                <i class="lni lni-close"></i>
            </button>
        </div>
        <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
                  <h5>Application Cover Letter</h5>
                  {!! $app->cover_letter !!}

              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" onclick="event.preventDefault(); document.getElementById('apply-form{{ $app->id }}').submit();" class="btn btn-success">Apply Now <i class="bx bx-right-arrow-alt" style="vertical-align: middle;"></i></button>
        </div>
      </div>
    </div>
  </div>

