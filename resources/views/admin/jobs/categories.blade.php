@extends('layouts.master')
@section('title', 'Manage Categories')
@section('content')
    @include('inc.breadcrumb')
    <div class="manage-jobs section">
        <div class="container">
            <div class="alerts-inner">
                <div class="row">
                    @include('inc.dashboard.sidebar')
                    <div class="col-lg-8 col-12">
                        @include('inc.dashboard.alert')
                        <div class="job-items">
                            @if(count($cats) > 0)
                                <div class="manage-list">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Category Name</p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Date Created</p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Status</p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Actions</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @forelse($cats as $cat)
                                <div class="manage-content">
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <h3>{{ $cat->cat_name }}</h3>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>
                                                {{ $cat->created_at->format('dS M, Y') }}
                                            </p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>
                                                @if($cat->status == '1')
                                                    Active
                                                @else
                                                    Inactive
                                                @endif
                                            </p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>
                                                <a href="{{ route('dashboard.admin.category.edit', $cat->cat_id) }}" class="mx-2"><i class="lni lni-pencil"></i> Edit</a>
                                                <a href="{{ route('dashboard.admin.category.delete', $cat->cat_id) }}" onclick="return confirm('Are you sure you want to delete this category?');" class="delete mx-2"><i class="lni lni-trash"></i> Delete</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="manage-content">
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-lg-12 col-12">
                                            <h4>Oops!! No Job Categories Found.</h4>
                                        </div>
                                    </div>
                                </div>
                            @endforelse
                        </div>

                        <div class="pagination left pagination-md-center">
                            <ul class="pagination-list">
                                {!! $cats->links() !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <!-- Dashboard -->
    <div id="dashboard">

        <!-- Navigation
        ================================================== -->
    @include('inc.dashboard.sidebar')
    <!-- Content
	================================================== -->
        <div class="dashboard-content">
            @include('inc.dashboard.breadcrumb')


            <div class="row">

                <!-- Table-->
                <div class="col-lg-12 col-md-12">

                   @include('inc.dashboard.alert')
                    <div class="dashboard-list-box margin-top-30">
                        <div class="dashboard-list-box-content">

                            <!-- Table -->

                            <table class="manage-table responsive-table">
                                @if(count($cats) > 0)
                                <tr>
                                    <th><i class="fa fa-file-text"></i> Category Name</th>
                                    <th><i class="fa fa-calendar"></i> Date Created</th>
                                    <th><i class="fa fa-calendar"></i> Status</th>
                                    <th></th>
                                </tr>
                                @endif
                                @forelse($cats as $cat)
                                <!-- Item #2 -->
                                <tr>
                                    <td class="title"><a href="#">{{ $cat->cat_name }}</a></td>
                                    <td>{{ $cat->created_at->format('dS M, Y') }}</td>
                                    <td class="centered">
                                        @if($cat->status == '1')
                                            Active
                                        @else
                                            Inactive
                                        @endif
                                    </td>
                                    <td class="action">
                                        <a href="{{ route('dashboard.admin.category.edit', $cat->cat_id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                        <a href="#"><i class="fa  fa-check "></i> Assign To</a>
                                        <a href="{{ route('dashboard.admin.category.delete', $cat->cat_id) }}" onclick="return confirm('Are you sure you want to delete this job?');" class="delete"><i class="fa fa-remove"></i> Delete</a>
                                    </td>
                                </tr>
                                @empty

                                <!-- Item #2 -->
                                    <tr>
                                        <td class="title"><a href="#">Oops!! No Job Categories Found.</a></td>
                                    </tr>
                                @endforelse


                                    <div class="pagination-container">
                                        <nav class="pagination">
                                            {!! $cats->links() !!}
                                        </nav>

                                        {{--                <nav class="pagination-next-prev">--}}
                                        {{--                    <ul>--}}
                                        {{--                        <li><a href="#" class="prev">Previous</a></li>--}}
                                        {{--                        <li><a  2href="#" class="next">Next</a></li>--}}
                                        {{--                    </ul>--}}
                                        {{--                </nav>--}}
                                    </div>
                            </table>

                        </div>
                    </div>
                    <a href="{{ route('dashboard.admin.category.add') }}" class="button margin-top-30">Add New Category</a>
                </div>

                @include('inc.dashboard.footer')
            </div>

        </div>
        <!-- Content / End -->

    </div>
    <!-- Dashboard / End -->
@endsection
