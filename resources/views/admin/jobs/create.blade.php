@extends('layouts.master')
@isset($job->id)
@section('title', \Str::limit($job->title, 25))
@else
@section('title', 'Post Job')
@endisset
@section('content')
    @include('inc.breadcrumb')
    <section class="job-post section">
        <div class="container">
            <div class="row">
                @include('inc.dashboard.sidebar')
                <div class="col-lg-8 col-12">
                    <div class="job-information">
                        <h3 class="title">Job Information</h3>
                        @include('inc.dashboard.alert')
                        @isset($job->id)
                            <form method="POST" action="{{ route('dashboard.admin.jobs.update', $job->id) }}">
                                @method('patch')
                                @else
                                    <form method="POST" action="{{ route('dashboard.admin.jobs.store') }}">
                                        @endisset
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Job title*</label>
                                                    <input class="form-control" type="text"  name="title"  value="{{ old('title', $job->title) }}"/>
                                                </div>
                                                @error('title')
                                                <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Category*</label>
                                                    <select class="select" name="cat_id">
                                                        @forelse (\App\Models\JobCategory::active()->get() as $cat)
                                                            <option value="{{ $cat->cat_id }}" @if($job->cat_id == $cat->cat_id) selected @endif>{{ $cat->cat_name }}</option>
                                                        @empty
                                                            <option value="0">Uncategorized</option>
                                                        @endforelse
                                                    </select>
                                                </div>
                                                @error('cat_id')
                                                <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Job Types*</label>
                                                    <select class="select" name="type_id">
                                                        @forelse (\App\Models\JobType::active()->get() as $type)
                                                            <option value="{{ $type->type_id }}" @if($job->type_id == $type->type_id) selected @endif>{{ $type->type_name }}</option>
                                                        @empty
                                                            <option value="0">Full-Time</option>
                                                        @endforelse
                                                    </select>
                                                </div>
                                                @error('type_id')
                                                <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Application Deadline</label>
                                                    <div class="input-group date">
                                                        <input type="text" class="form-control" name="close_date" value="{{ old('close_date', $job->close_date) }}"  placeholder="yyyy-mm-dd"/>
                                                        <span class="input-group-addon"></span>
                                                        <i class="bx bx-calendar"></i>
                                                    </div>
                                                </div>
                                                @error('close_date')
                                                <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Experience <span>(Years)</span></label>
                                                    <div class="input-group date">
                                                        <input type="number" placeholder="e.g 1 or 4" class="form-control" name="experience" value="{{ old('experience', $job->experience) }}"/>
                                                        <span class="input-group-addon"></span>
                                                        <i class="bx bx-calendar"></i>
                                                    </div>
                                                </div>
                                                @error('experience')
                                                <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Job Description*</label>
                                                    <textarea class="form-control" name="description" rows="5">{{ old('description', $job->description) }}</textarea>
                                                </div>
                                                @error('description')
                                                <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <h3 class="title">More Details</h3>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Location (Address)</label>
                                                    <input class="form-control" type="text"  name="location" placeholder="e.g. Newark, NJ" value="{{ old('location', $job->location) }}"/>
                                                </div>
                                                @error('location')
                                                <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Wages*</label>
                                                    <input class="form-control" type="text"  name="wages"  value="{{ old('wages', $job->wages) }}"/>
                                                </div>
                                                @error('wages')
                                                <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Duration*</label>
                                                    <input class="form-control" type="text" placeholder="e.g hourly, yearly, weekly" value="{{ old('duration', $job->duration) }}" name="duration"/>

                                                    @error('duration')
                                                    <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-12 my-3">
                                                <!-- Status -->
                                                <div class="form-check-inline">
                                                    <label>Status*</label>
                                                    <label style="display: inline-block !important;" class="mx-3" for="status-publish">
                                                        <input type="radio" value="1" id="status-publish" @if(old('status', $job->status) == '1') checked @endif name="status">
                                                        Publish
                                                    </label>
                                                    <label style="display: inline-block !important;" class="mx-3" for="status-draft">
                                                        <input type="radio" value="0" id="status-draft" @if(old('status', $job->status) == '0') checked @endif name="status">
                                                        Draft
                                                    </label>
                                                    @error('status')
                                                    <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 button">
                                                <button type="submit" class="btn">Save Job</button>
                                            </div>
                                        </div>
                                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
