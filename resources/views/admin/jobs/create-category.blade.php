@extends('layouts.master')
@isset($jobCategory->cat_id)
@section('title', \Str::limit($jobCategory->cat_name, 25))
@else
@section('title', 'Add Job category')
@endisset
@section('content')
    @include('inc.breadcrumb')
    <section class="job-post section">
        <div class="container">
            <div class="row">
                @include('inc.dashboard.sidebar')
                <div class="col-lg-8 col-12">
                    <div class="job-information">
                        <h3 class="title">Category Details</h3>
                        @include('inc.dashboard.alert')
                        @isset($jobCategory->cat_id)
                            <form class="col-lg-12 col-md-12" method="POST" action="{{ route('dashboard.admin.category.update', $jobCategory->cat_id) }}">
                                @method('patch')
                        @else
                            <form class="col-lg-12 col-md-12" method="POST" action="{{ route('dashboard.admin.category.store') }}">
                        @endisset
                                        @csrf
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Category Name</label>
                                                    <input class="form-control" type="text"  name="cat_name" placeholder="" value="{{ old('cat_name', $jobCategory->cat_name) }}"/>
                                                </div>


                                                @error('cat_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-12 my-3">
                                                <!-- Status -->
                                                <div class="form-check-inline">
                                                    <label>Category Status*</label>
                                                    <label style="display: inline-block !important;" class="mx-3" for="status-publish">
                                                        <input type="radio" value="1" id="status-publish" @if(old('status', $jobCategory->status) == '1') checked @endif name="status">
                                                        Publish
                                                    </label>
                                                    <label style="display: inline-block !important;" class="mx-3" for="status-draft">
                                                        <input type="radio" value="0" id="status-draft" @if(old('status', $jobCategory->status) == '0') checked @endif name="status">
                                                        Draft
                                                    </label>
                                                    @error('status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <b>{{ $message }}</b>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 button">
                                                <button type="submit" class="btn">Save Category</button>
                                            </div>
                                        </div>
                                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
