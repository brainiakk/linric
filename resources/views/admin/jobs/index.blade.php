@extends('layouts.master')
@section('title', 'Manage Jobs')
@section('content')
    @include('inc.breadcrumb')
    <div class="manage-jobs section">
        <div class="container">
            <div class="alerts-inner">
                <div class="row">
                    @include('inc.dashboard.sidebar')
                    <div class="col-lg-8 col-12">
                        @include('inc.dashboard.alert')
                        <div class="job-items">
                            @if(count($jobs) > 0)
                            <div class="manage-list">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p>Contract Type</p>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p>Applications</p>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p>Actions</p>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @forelse($jobs as $job)
                            <div class="manage-content">
                                <div class="row align-items-center justify-content-center">
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <h3>{{ $job->title }}</h3>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p><span class="time">{{ $job->type->type_name }}</span></p>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p>
                                            {{ count($job->application()->get()) > 0 ? count($job->application()->get()) : 0 }}
                                        </p>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p>
                                            <a href="{{ route('dashboard.admin.jobs.edit', $job->id) }}" class="mx-2"><i class="lni lni-pencil"></i> Edit</a>
                                            <a href="{{ route('dashboard.admin.jobs.delete', $job->id) }}" onclick="return confirm('Are you sure you want to delete this job?');" class="delete mx-2"><i class="lni lni-trash"></i> Delete</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @empty
                            <div class="manage-content">
                                <div class="row align-items-center justify-content-center">
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <h3>UI/UX designer</h3>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p><span class="time">Full-Time</span></p>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <div class="can-img">
                                            <a href="#"><img src="assets/images/jobs/candidates.png" alt="#" /></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-12">
                                        <p><i class="lni lni-star"></i></p>
                                    </div>
                                </div>
                            </div>
                            @endforelse
                        </div>

                        <div class="pagination left pagination-md-center">
                            <ul class="pagination-list">
                                {!! $jobs->links() !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
