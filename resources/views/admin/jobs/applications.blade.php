@extends('layouts.master')
@isset($job->id)
    @section('title', $job->title.' Job Applications')
@else
    @section('title', 'Manage Job Applications')
@endisset
@section('content')
    @include('inc.breadcrumb')
    <div class="manage-applications section">
        <div class="container">
            <div class="alerts-inner">
                <div class="row">
                    @include('inc.dashboard.sidebar')
                    <div class="col-lg-8 col-12">
                        <div class="job-items">
                            @forelse ($applications as $app)
                                <div class="manage-content">
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-lg-4 col-md-4">
                                            <div class="title-img">
                                                <h3>
                                                    {{ $app->user->name() }} <span><a href="tel:{{ $app->user->tel }}"><i class="fa fa-phone"></i> Call: {{ $app->user->tel }}</a></span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <p>
                                                <a href="{{ asset($app->resume) }}" target="_blank"> Resume</a>
                                                <a href="#applicaionCL{{ $app->id }}"  data-toggle="modal" data-target="#applicaionCL{{ $app->id }}"><i class="lni lni-empty-file"></i> Cover Letter</a>
                                            </p>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <p>
                                                @if($app->status == '1')
                                                    Pending
                                                @elseif($app->status == '2')
                                                    Approved
                                                @else
                                                    Rejected
                                                @endif
                                            </p>
                                        </div>
                                        <div class="col-lg-2 col-md-2">
                                            <a href="#" onclick=" event.preventDefault(); document.querySelector('#delete{{$app->id}}').click();">
                                                <i class="lni lni-trash"></i>
                                            </a>

                                            @if(\App\Models\JobAssigned::where('user_id', $app->user->user_id)->where('job_id', $app->job->id)->first())
                                                <a href="#" class=""><i class="lni lni-checkmark-circle"></i> Approved</a>
                                            @else
                                                <a href="{{ route('dashboard.admin.jobs.applications.accept', $app->id) }}"><i class="lni lni-checkmark"></i> Approve</a>
                                            @endif
                                        </div>
                                    </div>
                                    <a href="{{ route('dashboard.admin.jobs.applications.delete', $app->id) }}" id="delete{{$app->id}}"></a>
                                </div>
                                @push('apply-modal')
                                    @include('admin.jobs.cover-modal')
                                @endpush
                            @empty
                                <div class="manage-content">
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-lg-12">
                                            <div class="title-img">
                                                <h3>Oops!! You have no job applications at this time.</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforelse
                        </div>

                        <div class="pagination left pagination-md-center">
                            <ul class="pagination-list">
                                {!! $applications->links() !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Dashboard -->
    <div id="dashboard">

        <!-- Navigation
        ================================================== -->
    @include('inc.dashboard.sidebar', ['page' => 'Jobs'])
    <!-- Content
	================================================== -->
        <div class="dashboard-content">
            @include('inc.dashboard.breadcrumb')

            <div class="row">
                @isset($job->id)
                <!-- Table-->
                <div class="col-lg-12 col-md-12">

                    <div class="notification notice">
                        The job applications for <strong><a href="#">{{ $job->title }}</a></strong> are listed below.
                    </div>
                </div>
                @endisset

                <div class="col-md-6">
                    <!-- Select -->
                    <select data-placeholder="Filter by status" class="chosen-select-no-single">
                        <option value="">Filter by status</option>
                        <option value="new">New</option>
                        <option value="interviewed">Interviewed</option>
                        <option value="offer">Offer extended</option>
                        <option value="hired">Hired</option>
                        <option value="archived">Archived</option>
                    </select>
                    <div class="margin-bottom-15"></div>
                </div>

                <div class="col-md-6">
                    <!-- Select -->
                    <select data-placeholder="Newest first" class="chosen-select-no-single">
                        <option value="desc">Newest first</option>
                        <option value="name">Sort by name</option>
                    </select>
                    <div class="margin-bottom-35"></div>
                </div>


                <!-- Applications -->
                <div class="col-md-12">
                    @include('inc.dashboard.alert')
                    @forelse($applications as $app)
                    <!-- Application #1 -->
                    <div class="application">
                        <div class="app-content">

                            <!-- Name / Avatar -->
                            <div class="info">
                                <img src="{{ $app->user->avatar() }}" alt="">
                                <span>{{ $app->user->name() }}</span>
                                <ul>
                                    <li><a href="{{ asset($app->resume) }}" target="_blank"><i class="fa fa-file-text"></i> Download CV</a></li>
                                    <li><a href="tel:{{ $app->user->tel }}"><i class="fa fa-phone"></i> Contact</a></li>
                                </ul>
                            </div>

                            <!-- Buttons -->
                            <div class="buttons">
                                <a href="#one-1" class="button gray app-link"><i class="fa fa-pencil"></i> Edit Status</a>
                                <a href="#three-1" class="button gray app-link"><i class="fa fa-plus-circle"></i> Show Details</a>
                            </div>
                            <div class="clearfix"></div>

                        </div>

                        <!--  Hidden Tabs -->
                        <div class="app-tabs">

                            <a href="#" class="close-tab button gray"><i class="fa fa-close"></i></a>

                            <!-- First Tab -->
                            <div class="app-tab-content" id="one-1">
                                <div class="clearfix"></div>
                                @if(\App\Models\JobAssigned::where('user_id', $app->user->user_id)->where('job_id', $app->job->id)->first())
                                <a href="#" class="button margin-top-15">Application Already Accepted</a>
                                @else
                                <a href="{{ route('dashboard.admin.jobs.applications.accept', $app->id) }}" class="button margin-top-15">Accept Application / Hire</a>
                                @endif
                                <a href="{{ route('dashboard.admin.jobs.applications.delete', $app->id) }}" class="button gray margin-top-15 delete-application">Delete this application</a>

                            </div>

                            <!-- Third Tab -->
                            <div class="app-tab-content"  id="three-1">
                                <i>Full Name:</i>
                                <span>{{ $app->user->name() }}</span>

                                <i>Email:</i>
                                <span><a href="mailto:{{ $app->user->email }}">{{ $app->user->email }}</a></span>

                                <i>Message:</i>
                                <span>
                                    {{ $app->cover_letter }}
                                </span>
                            </div>

                        </div>

                        <!-- Footer -->
                        <div class="app-footer">

                            <ul>
                                <li><i class="fa fa-calendar"></i> {{ $app->created_at->format('F d, Y') }}</li>
                            </ul>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    @empty
                    <div class="application">
                        <div class="app-content">
                            Oops!! No Job Applications has been submitted yet.
                        </div>
                    </div>
                    @endforelse

                </div>

                @include('inc.dashboard.footer')
            </div>

        </div>
        <!-- Content / End -->

    </div>
    <!-- Dashboard / End -->
@endsection
