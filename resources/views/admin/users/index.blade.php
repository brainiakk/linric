@extends('layouts.master')
@section('title', 'Manage Users')
@section('content')
    @include('inc.breadcrumb')
    <div class="manage-jobs section">
        <div class="container">
            <div class="alerts-inner">
                <div class="row">
                    @include('inc.dashboard.sidebar')
                    <div class="col-lg-8 col-12">
                        @include('inc.dashboard.alert')
                        <div class="job-items">
                            @if(count($users) > 0)
                                <div class="manage-list">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Full Name</p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Telephone</p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Status</p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p>Actions</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @forelse($users as $user)
                                <div class="manage-content">
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <h3>{{ $user->name() }}<span><small>{{ $user->email }}</small></span></h3>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-12">
                                            <p><span class="time">{{ $user->tel }}</span></p>
                                        </div>
                                        <div class="col-lg-1 col-md-1 col-12">
                                            <p>
                                                @if($user->status == '1')
                                                    Active
                                                @else
                                                    Inactive
                                                @endif
                                            </p>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-12">
                                            <p>
                                                <a href="{{ route('dashboard.admin.users.edit', $user->user_id) }}" class="mx-2"><i class="lni lni-pencil"></i> Edit</a>
                                                @if($user->status == '1')
                                                    <a href="{{ route('dashboard.admin.users.deactivate', $user->user_id) }}"><i class="lni lni-close"></i> Deactivate</a>
                                                @else
                                                    <a href="{{ route('dashboard.admin.users.activate', $user->user_id) }}"><i class="lni lni-eye"></i> Activate</a>
                                                @endif
                                                <a href="{{ route('dashboard.admin.users.delete', $user->user_id) }}" onclick="return confirm('Are you sure you want to delete this user?');" class="delete mx-2"><i class="lni lni-trash"></i> Delete</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="manage-content">
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-lg-12 col-12">
                                            <h3>Oops!! No Users Found.</h3>
                                        </div>
                                    </div>
                                </div>
                            @endforelse
                        </div>

                        <div class="pagination left pagination-md-center">
                            <ul class="pagination-list">
                                {!! $users->links() !!}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Dashboard -->
    <div id="dashboard">

        <!-- Navigation
        ================================================== -->
    @include('inc.dashboard.sidebar')
    <!-- Content
	================================================== -->
        <div class="dashboard-content">
            @include('inc.dashboard.breadcrumb')


            <div class="row">

                <!-- Table-->
                <div class="col-lg-12 col-md-12">

                    @include('inc.dashboard.alert')
                    <div class="dashboard-list-box margin-top-30">
                        <div class="dashboard-list-box-content">

                            <!-- Table -->

                            <table class="manage-table responsive-table">
                                @if(count($users) > 0)
                                    <tr>
                                        <th><i class="fa fa-file-text"></i> Full Name</th>
                                        <th><i class="fa fa-mail-reply"></i>Email</th>
                                        <th><i class="fa fa-calendar"></i> Status</th>
                                        <th><i class="fa fa-calendar"></i> Date Joined</th>
                                        <th><i class="fa fa-user"></i> Level</th>
                                        <th></th>
                                    </tr>
                                @endif
                                @forelse($users as $user)
                                <!-- Item #2 -->
                                    <tr>
                                        <td class="title">
                                            <a href="#">{{ $user->fname }} @isset($user->mname) {{ $user->mname }} @endisset @isset($user->lname) {{ $user->lname }} @endisset</a>
                                        </td>
                                        <td class="centered">{{ $user->email }}</td>
                                        <td>
                                            @if($user->status == '1')
                                                Active
                                            @else
                                                Inactive
                                            @endif
                                        </td>
                                        <td>{{ $user->created_at->format('dS M, Y') }}</td>
                                        <td class="centered">
                                            @if($user->level > 0)
                                                Admin
                                            @else
                                                User
                                            @endif
                                        </td>
                                        <td class="action">
                                            <a href="{{ route('dashboard.admin.users.edit', $user->user_id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                            @if($user->status == '1')
                                                <a href="{{ route('dashboard.admin.users.deactivate', $user->user_id) }}"><i class="fa  fa-eye-slash"></i> Deactivate</a>
                                            @else
                                                <a href="{{ route('dashboard.admin.users.activate', $user->user_id) }}"><i class="fa  fa-eye-slash"></i> Activate</a>
                                            @endif
                                            <a href="{{ route('dashboard.admin.users.delete', $user->user_id) }}" onclick="return confirm('Are you sure you want to delete this job?');" class="delete"><i class="fa fa-remove"></i> Delete</a>
                                        </td>
                                    </tr>
                                @empty

                                <!-- Item #2 -->
                                    <tr>
                                        <td class="title"><a href="#">Oops!! No Users Found.</a></td>
                                    </tr>
                                @endforelse


                            </table>

                        </div>
                    </div>
                </div>

                @include('inc.dashboard.footer')
            </div>

        </div>
        <!-- Content / End -->

    </div>
    <!-- Dashboard / End -->
@endsection
