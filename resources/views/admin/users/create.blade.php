@extends('layouts.master')
@isset($user->user_id)
@section('title', \Str::limit($user->name(), 25))
@else
@section('title', 'Create User')
@endisset
@section('content')
    @include('inc.breadcrumb')
    <section class="job-post section">
        <div class="container">
            <div class="row">
                @include('inc.dashboard.sidebar')
                <div class="col-lg-8 col-12">
                    <div class="job-information">
                        <h3 class="title">User Details</h3>
                        @include('inc.dashboard.alert')
                        @isset($user->user_id)
                            <form class="col-lg-12 col-md-12" method="POST" action="{{ route('dashboard.admin.users.update', $user->user_id) }}">
                                @method('patch')
                        @else
                            <form class="col-lg-12 col-md-12" method="POST" action="{{ route('dashboard.admin.users.store') }}">
                        @endisset
                                @csrf
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>First Name*</label>
                                            <input class="form-control" type="text"  name="fname"  value="{{ old('fname', $user->fname) }}"/>
                                        </div>
                                        @error('fname')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Middle Name</label>
                                            <input class="form-control" type="text"  name="mname"  value="{{ old('mname', $user->mname) }}"/>
                                        </div>
                                        @error('mname')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Last Name*</label>
                                            <input class="form-control" type="text"  name="lname"  value="{{ old('lname', $user->lname) }}"/>
                                        </div>
                                        @error('lname')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Email Address*</label>
                                            <input class="form-control" type="email"  name="email"  value="{{ old('email', $user->email) }}"/>
                                        </div>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>User Level*</label>
                                            <select class="select" name="level">
                                                <option value=""></option>
                                                <option @if($user->level == '1') selected @endif value="1">Admin</option>
                                                <option @if($user->level == '0') selected @endif value="0">User</option>
                                            </select>
                                        </div>
                                        @error('level')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Gender*</label>
                                            <select class="select" name="gender">
                                                <option value=""></option>
                                                <option value="f" @if($user->gender == 'f') selected @endif>Female</option>
                                                <option value="m" @if($user->gender == 'm') selected @endif>Male</option>
                                            </select>
                                        </div>
                                        @error('gender')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <label>Telephone Number</label>
                                            <div class="input-group date">
                                                <input type="tel" class="form-control" name="tel" value="{{ old('tel', $user->tel) }}"  placeholder="929293290"/>
                                            </div>
                                        </div>
                                        @error('tel')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                @if(!isset($user->user_id))
                                <h3 class="title">Security</h3>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Password*</label>
                                            <input class="form-control" type="password"  name="password"  placeholder="***********" value="{{ old('new-password') }}"/>
                                        </div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Confirm Password*</label>
                                            <input class="form-control" type="password" placeholder="***********"  name="password_confirmation"/>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-lg-12 my-3">
                                        <!-- Status -->
                                        <div class="form-check-inline">
                                            <label>Status*</label>
                                            <label style="display: inline-block !important;" class="mx-3" for="status-publish">
                                                <input type="radio" value="1" id="status-publish" @if(old('status', $user->status) == '1') checked @endif name="status">
                                                Publish
                                            </label>
                                            <label style="display: inline-block !important;" class="mx-3" for="status-draft">
                                                <input type="radio" value="0" id="status-draft" @if(old('status', $user->status) == '0') checked @endif name="status">
                                                Draft
                                            </label>
                                        </div>
                                        @error('status')
                                        <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12 button">
                                        <button type="submit" class="btn">Save </button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
