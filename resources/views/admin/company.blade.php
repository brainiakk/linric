@extends('layouts.master')
@section('title', 'Site Settings')
@section('content')
<!-- Dashboard -->
<div id="dashboard">

    <!-- Navigation
    ================================================== -->
    @include('inc.dashboard.sidebar')
    <!-- Content
	================================================== -->
    <div class="dashboard-content">
        @include('inc.dashboard.breadcrumb', ['page' => 'Settings'])


        <div class="row">

                <form class="col-lg-12 col-md-12" method="POST" action="{{ route('dashboard.admin.company.save') }}">
                    @csrf
                    <!-- Table-->

                    @include('inc.dashboard.alert')
                    <div class="dashboard-list-box margin-top-0">
                        <h4>Website Details</h4>
                        <div class="dashboard-list-box-content">

                            <div class="submit-page">

                                <!-- Title -->
                                <div class="form">
                                    <h5>Company Name</h5>
                                    <input class="search-field" name="name" type="text" placeholder="" value="{{ old('name', $company->name) }}"/>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>

                                <!-- Official Email Address -->
                                <div class="form">
                                    <h5>Official Email Address</h5>
                                    <input class="search-field" name="email" type="email" placeholder="" value="{{ old('email', $company->email) }}"/>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>

                                <!-- Official Tel -->
                                <div class="form">
                                    <h5>Official Telephone Number</h5>
                                    <input class="search-field" name="tel" type="tel" placeholder="" value="{{ old('tel', $company->tel) }}"/>

                                    @error('tel')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>


                                <!-- Location -->
                                <div class="form">
                                    <h5>Address </h5>
                                    <input class="search-field" type="text" name="address" placeholder="e.g. Newark, NJ" value="{{ old('address', $company->address) }}"/>
                                    <p class="note">Leave this blank if the location is not important</p>

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>


                                <!-- City -->
                                <div class="form">
                                    <h5>City </h5>
                                    <input class="search-field" type="text" name="city" placeholder="e.g. Newark, NJ" value="{{ old('city', $company->city) }}"/>


                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>

                                <!-- City -->
                                <div class="form">
                                    <h5>Country </h5>
                                    <input class="search-field" type="text" name="country" placeholder="e.g. U.S.A" value="{{ old('country', $company->country) }}"/>

                                    @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>



                                <!-- Description -->
                                <div class="form" style="width: 100%;">
                                    <h5>About</h5>
                                    <textarea class="WYSIWYG" name="about" cols="40" rows="3" id="summary" spellcheck="true">{{ old('about', $company->about) }}</textarea>

                                    @error('about')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>


                            </div>

                        </div>
                    </div>


                    <div class="dashboard-list-box margin-top-30">
                        <h4>Social Media Links</h4>
                        <div class="dashboard-list-box-content">

                            <div class="submit-page">

                                <!-- Company Name -->
                                <div class="form">
                                    <h5>Facebook <span>(Optional)</span></h5>
                                    <input type="url" placeholder="e.g 1 or 4" value="{{ old('fb_link', $company->fb_link) }}" name="fb_link">
                                </div>

                                <!-- Website -->
                                <div class="form">
                                    <h5>Twitter <span>(Optional)</span></h5>
                                    <input type="url" placeholder="e.g 20" value="{{ old('tw_link', $company->tw_link) }}" name="tw_link">

                                    @error('tw_link')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>

                                <!-- Teagline -->
                                <div class="form">
                                    <h5>Google Plus <span>(Optional)</span></h5>
                                    <input type="url" placeholder="e.g hourly, yearly, weeklq" value="{{ old('go_link', $company->go_link) }}" name="go_link">

                                    @error('go_link')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>

                                <!-- Teagline -->
                                <div class="form">
                                    <h5>LinkedIn <span>(Optional)</span></h5>
                                    <input type="url" placeholder="e.g hourly, yearly, weekly" value="{{ old('li_link', $company->li_link) }}" name="li_link">

                                    @error('li_link')
                                    <span class="invalid-feedback" role="alert">
                                        <b>{{ $message }}</b>
                                    </span>
                                    @enderror
                                </div>


                            </div>

                        </div>
                    </div>
                    <button type="submit" class="button margin-top-30">Save <i class="fa fa-arrow-circle-right"></i></button>
                </form>
        </div>

        @include('inc.dashboard.footer')
    </div>

</div>
<!-- Content / End -->

</div>
<!-- Dashboard / End -->
@endsection
