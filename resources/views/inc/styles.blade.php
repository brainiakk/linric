
<link rel="stylesheet" href="{{ asset('assets/css/fonts.css') }}">

<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/LineIcons.2.0.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/tiny-slider.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/glightbox.min.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0">
