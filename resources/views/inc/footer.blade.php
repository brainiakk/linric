<footer class="footer">
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="f-about single-footer">
                        <div class="logo">
                            <a href="{{ route('home') }}"><img style="height: 55px; width: auto;" src="{{ asset('assets/images/logo/logo2.png') }}" alt="{{ $appName }}" /></a>
                        </div>
                        <p>

                        </p>
                        <ul class="contact-address">
                            <li><span>Address:</span> {{ $appAddress }}</li>
                            <li>
                                <span>Email:</span>
                                <a href="mailto:{{ $appEmail }}">{{ $appEmail }}</a>
                            </li>
                            <li><span>Call:</span><a href="tel:{{ $appTel }}">{{ $appTel }}</a></li>
                        </ul>
                        <div class="footer-social">
                            <ul>
                                <li>
                                    <a href="#"><i class="lni lni-facebook-original"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="lni lni-twitter-original"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="lni lni-linkedin-original"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="lni lni-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="row">
                        <div class="col-lg-5 col-md-6 col-12">
                            <div class="single-footer f-link">
                                <h3>Sitemap</h3>
                                <ul>
                                    <li><a href="{{ route('jobs.home') }}">Job Openings</a></li>
                                    <li><a href="#">My Resume</a></li>
                                    <li><a href="{{ route('about') }}">About Us</a></li>
                                    <li><a href="{{ route('contact') }}">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6 col-12">
                            <div class="single-footer newsletter">
                                <h3>Join Our Newsletter</h3>
                                <p>
                                    Subscribe to get the latest jobs posted, candidates...
                                </p>
                                <form action="#" method="POST" class="newsletter-inner">
                                    <input  placeholder="Your email address" class="common-input"
                                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email address'" required=""
                                            type="email" />
                                    <div class="button">
                                        <button class="btn">
                                            Subscribe Now! <span class="dir-part"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="inner">
                <div class="row">
                    <div class="col-12">
                        <div class="left">
                            <p>
                                Designed and Developed by <a href="{{ $appCl }}" target="_blank">{{ $appCr }}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
