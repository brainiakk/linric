<!-- Titlebar -->
<div id="titlebar">
    <div class="row">
        <div class="col-md-12">
            <h2>@yield('title')</h2>
            <!-- Breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Dashboard</a></li>
                    @isset($page)
                    <li>{{ $page }}</li>
                    @endisset
                    <li>@yield('title')</li>
                </ul>
            </nav>
        </div>
    </div>
</div>
