<!-- Copyrights -->
<div class="col-md-12">
    <div class="copyrights">© {{ date('Y').' '. $appName }}. All Rights Reserved.</div>
</div>
