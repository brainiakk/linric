<div class="col-lg-4 col-12">
    <div class="dashbord-sidebar">
        <ul>
            <li class="heading">Manage Account</li>
            <li>
                <a @if(\Route::is('dashboard.profile.user')) class="active" @endif href="{{ route('dashboard.profile.user') }}"><i class="lni lni-user"></i> My Profile</a>
            </li>
            <li> <a @if(\Route::is('dashboard.jobs.applied')) class="active" @endif href="{{ route('dashboard.jobs.applied') }}"><i class="lni lni-envelope"></i> Manage
                    Applications</a>
            </li>
            @can('admin')
            <li>
                <a @if(\Route::is('dashboard.admin.category.add')) class="active" @endif href="{{ route('dashboard.admin.category.add') }}"><i class="lni lni-circle-plus"></i> New Category</a>
            </li>

            <li>
                <a @if(\Route::is('dashboard.admin.category.home')) class="active" @endif href="{{ route('dashboard.admin.category.home') }}"><i class="lni lni-grid-alt"></i> Manage Categories</a>
            </li>

            <li>
                <a @if(\Route::is('dashboard.admin.jobs.add')) class="active" @endif href="{{ route('dashboard.admin.jobs.add') }}"><i class="lni lni-add-files"></i> Post Job</a>
            </li>

            <li>
                <a @if(\Route::is('dashboard.admin.jobs.home')) class="active" @endif href="{{ route('dashboard.admin.jobs.home') }}"><i class="lni lni-briefcase"></i> Manage Jobs</a>
            </li>

            <li>
                <a @if(\Route::is('dashboard.admin.jobs.applications')) class="active" @endif href="{{ route('dashboard.admin.jobs.applications') }}"><i class="lni lni-folder"></i> Job Applications</a>
            </li>

            <li>
                <a @if(\Route::is('dashboard.admin.users.add')) class="active" @endif href="{{ route('dashboard.admin.users.add') }}"><i class="lni lni-user"></i> New User</a>
            </li>

            <li>
                <a @if(\Route::is('dashboard.admin.users.home')) class="active" @endif href="{{ route('dashboard.admin.users.home') }}"><i class="lni lni-users"></i> Manage Users</a>
            </li>
            @endcan
            <li>
                <a @if(\Route::is('dashboard.change.password')) class="active" @endif href="{{ route('dashboard.change.password') }}"><i class="lni lni-lock"></i> Change Password</a>
            </li>
            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="lni lni-upload"></i> Sign Out</a>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </ul>
    </div>
</div>
