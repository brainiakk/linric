@if(Session::has('success'))
    <div class="alert alert-success alert-block wow fadeOutDownBig" data-wow-delay=".6s">
        {{ Session::get('success') }}
    </div>
@elseif(Session::has('error'))
    <div class="alert alert-danger alert-block wow fadeOutDownBig" data-wow-delay=".9s">
        {{ Session::get('error') }}
    </div>
@endif
