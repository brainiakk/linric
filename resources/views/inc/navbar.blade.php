<header class="header @if(\Route::currentRouteName() != 'home') other-page @endif">
    <div class="navbar-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                            <span class="toggler-icon"></span>
                        </button>
                        <a class="navbar-brand logo" href="{{ route('home') }}">
                            <img class="logo1" style="height: 40px; width: auto;" src="{{ asset('assets/images/logo/logo2.png') }}" alt="{{ $appName }}" />
                        </a>
                        <div class="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                            <ul id="nav" class="navbar-nav ml-auto">
                                <li class="nav-item">
                                    <a @if(\Route::currentRouteName() == 'home')  class="active" @endif href="{{ route('home') }}">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a @if(\Route::is('jobs*'))  class="active" @endif href="{{ route('jobs.home') }}">Job Board</a>
                                </li>
                                <li class="nav-item">
                                    <a @if(\Route::currentRouteName() == 'about')  class="active" @endif href="{{ route('about') }}">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a @if(\Route::currentRouteName() == 'contact')  class="active" @endif href="{{ route('contact') }}">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                        @auth()
                        <div class="button">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                 document.getElementById('logout-form2').submit();" class="login"><i
                                    class="lni lni-upload"></i> Logout</a>
                            @can('admin')
                            <a href="{{ route('dashboard.admin.jobs.add') }}" class="btn">Post Job</a>
                            @else
                            <a href="{{ route('dashboard.profile.user') }}" class="btn">My Profile</a>
                            @endif
                            <form id="logout-form2" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                        @else
                        <div class="button">
                            <a href="{{ route('login') }}" class="login"><i
                                    class="lni lni-lock-alt"></i> Login</a>
                            <a href="{{ route('register') }}" class="btn">Sign Up</a>
                        </div>
                        @endauth
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

