<div class="breadcrumbs overlay">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumbs-content">
                    <h1 class="page-title">@yield('title')</h1>
                    <p>
                    </p>
                </div>
                <ul class="breadcrumb-nav">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>@yield('title')</li>
                </ul>
            </div>
        </div>
    </div>
</div>
