@extends('layouts.master')
@section('title', 'About Us')
@section('content')
<!-- Titlebar
================================================== -->
@include('inc.breadcrumb')
<!-- Content
================================================== -->
<section class="about-us section">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 col-md-10 col-12">
                <div class="content-right wow fadeInRight" data-wow-delay=".5s">
                    <h2>
                        About {{ $appName }}
                    </h2>
                    <p class="mb-4">
                        {{ $appName }} was founded in 2019. We assist organizations in connecting with the right
                        personnel by offering a platform for effective management and recruitment. We also assist
                        organizations in growing by matching the right people to their specialized staffing and
                        consulting needs, as well as assisting in the development of professionals careers for individuals.
                    </p>
                    <p class="mb-4">
                        In all we do, we adhere to a "Ethics First" concept. We also provide a work environment where
                        workers can prosper and develop while also being socially responsible corporate citizens and
                        active participants in the communities where they live and work.
                    </p>
                    <p class="mb-4">
                        What sets us apart is that we find jobs for people who have little or no experience by teaching
                        them to the appropriate professional level of abilities. Join us in our mission to transform the
                        workforce and communities.
                    </p>

                    <div class="single-list">
                        <i class="lni lni-users"></i>

                        <div class="list-bod">
                            <h5>Who We Are</h5>
                            <p>
                                {{ $appName }} founded in 2019, is an IT solutions advisor and provider specializing in
                                temporary staffing and Pro-Tech (Prospective Technology Communication System). We are
                                dedicated to assisting our clients in developing next-generation technological
                                solutions by connecting them with the right people.
                            </p>
                        </div>
                    </div>

                    <div class="single-list">
                        <i class="lni lni-checkbox"></i>

                        <div class="list-bod">
                            <h5>Why Choose Us</h5>
                            <p>
                                Our distinctiveness is demonstrated in our ability to find work for those with little
                                or no experience by teaching them to a professional level of proficiency. We want to
                                achieve community stability, lower crime and recidivism, and contribute to safer,
                                healthier communities by implementing this plan. Join us in our mission to transform
                                the workforce and communities in which we live.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-10 col-12">
                <div class="content-left wow fadeInLeft" data-wow-delay=".3s">
                    <div calss="row">
                        <div calss="col-lg-6 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-6">
                                    <img class="single-img" src="{{ asset('assets/images/about/small1.jpg') }}" alt="#" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-6">
                                    <img class="single-img mt-50" src="{{ asset('assets/images/about/small2.jpg') }}" alt="#" />
                                </div>
                            </div>
                        </div>
                        <div calss="col-lg-6 col-12">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-6">
                                    <img class="single-img minus-margin" src="{{ asset('assets/images/about/small3.jpg') }}" alt="#" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-6">
                                    <div class="media-body">
                                        <i class="lni lni-checkmark"></i>
                                        <h6 class="">Job alert!</h6>
                                        <p class="">104 new jobs are available in this week!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="apply-process section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
                <div class="process-item">
                    <i class="lni lni-user"></i>
                    <h4>Register Your Account</h4>
                    <p>
                        You can register a free account by visiting our <b>Sign Up</b> page, make sure to use verifiable details.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="process-item">
                    <i class="lni lni-book"></i>
                    <h4>Upload Your Resume</h4>
                    <p>
                        Upload your CV/resume when applying for your dream job or a job vacancy.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="process-item">
                    <i class="lni lni-briefcase"></i>
                    <h4>Apply for Dream Job</h4>
                    <p>
                        Fill the job application form with the necessary information, required of you for the job.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
