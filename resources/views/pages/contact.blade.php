@extends('layouts.master')
@section('title', 'Contact Us')
@section('content')

@include('inc.breadcrumb')
<section id="contact-us" class="contact-us section">
    <div class="container">
        <div class="contact-head wow fadeInUp" data-wow-delay=".4s">
            <div class="row">
                <div class="col-12">
                    <div class="form-main">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-block">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li style="list-style-type:none; ">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @include('inc.dashboard.alert')
                        <form class="form" method="post" action="{{ route('contact.send') }}">
                            @csrf
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <input  type="text" name="name" placeholder="Your Name" required="required" />
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" placeholder="Your Email" required="required" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group message">
                                        <textarea name="msg" placeholder="Your Message"></textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group button">
                                        <button type="submit" class="btn">
                                            Submit Message
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 m-auto">
                    <div class="single-head m-auto">
                        <div class="contant-inner-title">
                            <h4>Contact Information</h4>
                            <p>
                                You can reach out to us for enquiries (visit or call):
                            </p>
                        </div>
                        <div class="single-info">
                            <i class="lni lni-phone"></i>
                            <ul>
                                <li>{{ $appTel }}</li>
                            </ul>
                        </div>
                        <div class="single-info">
                            <i class="lni lni-envelope"></i>
                            <ul>
                                <li>
                                    <a href="#"><span
                                            class="__cf_email__"
                                        >{{ $appEmail }}</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="single-info">
                            <i class="lni lni-map"></i>
                            <ul>
                                <li>
                                    {{ $appAddress }}
                                </li>
                            </ul>
                        </div>
                        <div class="contact-social">
                            <h5>Follow Us on</h5>
                            <ul>
                                <li>
                                    <a href="#"><i class="lni lni-facebook-original"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="lni lni-twitter-original"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="lni lni-linkedin-original"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="lni lni-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
