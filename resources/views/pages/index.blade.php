@extends('layouts.master')
@section('title', 'Home')
@section('content')
<section class="hero-area">
    <div class="hero-inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 co-12">
                    <div class="hero-video-head wow fadeInRight" data-wow-delay=".5s">
                        <div class="video-inner">
                            <img src="{{ asset('assets/images/hero/hero-image.png') }}" alt="#" />

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 co-12">
                    <div class="inner-content">
                        <div class="hero-text">
                            <h1 class="wow fadeInUp" data-wow-delay=".3s">
                                Find Your Career <br />to Make a Better Life
                            </h1>
                            <p class="wow fadeInUp" data-wow-delay=".5s">
                                Find a Job, Work, or a Career
                            </p>
                        </div>
                        <div class="job-search-wrap-two mt-50 wow fadeInUp" data-wow-delay=".7s">
                            <div class="job-search-form">
                                <form action="{{ route('jobs.search') }}" method="POST">
                                    @csrf
                                    <div class="single-field-item keyword">
                                        <label for="keyword">What</label>
                                        <input id="keyword" placeholder="What jobs you want?" name="search" type="text" />
                                    </div>

                                    <div class="single-field-item location">
                                        <label for="location">Where</label>
                                        <input id="location" class="input-field input-field-location" placeholder="Location"
                                               name="location" type="text" />
                                    </div>

                                    <div class="submit-btn">
                                        <button class="btn" type="submit">Search</button>
                                    </div>
                                </form>
                            </div>
                            <div class="trending-keywords mt-30">
                                <div class="keywords style-two">
                                    <span class="title">Popular Keywords:</span>
                                    <ul>
                                        <li><a href="#" onclick="event.preventDefault(); searchCat('Administrative');">Administrative</a></li>
                                        <li><a href="#" onclick="event.preventDefault(); searchCat('Android');">Android</a></li>
                                        <li><a href="#" onclick="event.preventDefault(); searchCat('app');">app</a></li>
                                        <li><a href="#" onclick="event.preventDefault(); searchCat('ASP.NET');">ASP.NET</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="apply-process section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12">
                <div class="process-item">
                    <i class="lni lni-user"></i>
                    <h4>Register Your Account</h4>
                    <p>
                        You can register a free account by visiting our <b>Sign Up</b> page, make sure to use verifiable details.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="process-item">
                    <i class="lni lni-book"></i>
                    <h4>Upload Your Resume</h4>
                    <p>
                        Upload your CV/resume when applying for your dream job or a job vacancy.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="process-item">
                    <i class="lni lni-briefcase"></i>
                    <h4>Apply for Dream Job</h4>
                    <p>
                        Fill the job application form with the necessary information, required of you for the job.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="job-category section">
    <form action="{{ route('jobs.search') }}" method="POST" id="cat-filter-form">
        @csrf
        <input type="hidden" id="hiddenkeyword" name="filter">
    </form>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <span class="wow fadeInDown" data-wow-delay=".2s">Job Category</span>
                    <h2 class="wow fadeInUp" data-wow-delay=".4s">
                        Choose Your Desire Category
                    </h2>
                    <p class="wow fadeInUp" data-wow-delay=".6s">

                    </p>
                </div>
            </div>
        </div>
        <div class="cat-head">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('Technical Support');" class="single-cat wow fadeInUp" data-wow-delay=".2s">
                        <div class="icon">
                            <i class="lni lni-cog"></i>
                        </div>
                        <h3>
                            Technical<br />
                            Support
                        </h3>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('Business Development');" class="single-cat wow fadeInUp" data-wow-delay=".4s">
                        <div class="icon">
                            <i class="lni lni-layers"></i>
                        </div>
                        <h3>
                            Business<br />
                            Development
                        </h3>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('Real Estate Business');" class="single-cat wow fadeInUp" data-wow-delay=".6s">
                        <div class="icon">
                            <i class="lni lni-home"></i>
                        </div>
                        <h3>
                            Real Estate<br />
                            Business
                        </h3>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('Share Market Analysis');" class="single-cat wow fadeInUp" data-wow-delay=".8s">
                        <div class="icon">
                            <i class="lni lni-search"></i>
                        </div>
                        <h3>
                            Share Market<br />
                            Analysis
                        </h3>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('Finance & Banking Service');" class="single-cat wow fadeInUp" data-wow-delay=".2s">
                        <div class="icon">
                            <i class="lni lni-investment"></i>
                        </div>
                        <h3>
                            Finance & Banking <br />
                            Service
                        </h3>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('IT & Networking Services');" class="single-cat wow fadeInUp" data-wow-delay=".4s">
                        <div class="icon">
                            <i class="lni lni-cloud-network"></i>
                        </div>
                        <h3>
                            IT & Networking <br />
                            Services
                        </h3>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('Restaurant Services');" class="single-cat wow fadeInUp" data-wow-delay=".6s">
                        <div class="icon">
                            <i class="lni lni-restaurant"></i>
                        </div>
                        <h3>
                            Restaurant <br />
                            Services
                        </h3>
                    </a>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <a href="#" onclick="event.preventDefault(); searchCat('Defence & Fire Service');" class="single-cat wow fadeInUp" data-wow-delay=".8s">
                        <div class="icon">
                            <i class="lni lni-fireworks"></i>
                        </div>
                        <h3>
                            Defence & Fire <br />
                            Service
                        </h3>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="call-action overlay section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2 col-12">
                <div class="inner">
                    <div class="section-title">
                        <span class="wow fadeInDown" data-wow-delay=".2s">GETTING STARTED TO WORK</span>
                        <h2 class="wow fadeInUp" data-wow-delay=".4s">
                            Don’t just find. Be found. Put your CV in front of great
                            employers
                        </h2>
                        <p class="wow fadeInUp" data-wow-delay=".6s">
                            It helps you to increase your chances of finding a suitable
                            job and let recruiters contact you about jobs that are not
                            needed to pay for advertising.
                        </p>
                        <div class="button wow fadeInUp" data-wow-delay=".8s">
                            <a href="{{ route('dashboard.home') }}" class="btn"><i class="lni lni-upload"></i> Upload Your Resume</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if(count($jobs->get()) > 0)
<section class="find-job section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title">
                    <span class="wow fadeInDown" data-wow-delay=".2s">Hot Jobs</span>
                    <h2 class="wow fadeInUp" data-wow-delay=".4s">
                        Browse Recent Jobs
                    </h2>
                    <p class="wow fadeInUp" data-wow-delay=".6s">
                        There are many variations of passages of Lorem Ipsum available,
                        but the majority have suffered alteration in some form.
                    </p>
                </div>
            </div>
        </div>
        <div class="single-head">
            <div class="row">
                @forelse($jobs->limit(4)->get() as $job)
                <div class="col-lg-6 col-12">
                    <div class="single-job wow fadeInUp" data-wow-delay=".3s">
                        <div class="job-image">

                        </div>
                        <div class="job-content">
                            <h4><a href="#jobApply{{ $job->id }}" data-toggle="modal" data-target="#jobApply{{ $job->id }}">{{ $job->title }}</a></h4>
                            <p>
                                {{ $job->intro() }}
                            </p>
                            <ul>
                                <li>
                                    {{ $job->category->cat_name }}
                                </li>
                                <li><i class="lni lni-dollar"></i> {{ $job->wages }}/{{ $job->duration }}</li>
                                <li><i class="lni lni-map-marker"></i> {{ Str::limit($job->location, 15) }}</li>
                            </ul>
                        </div>
                        <div class="job-button">
                            <ul>
                                <li><a href="#jobApply{{ $job->id }}" data-toggle="modal" data-target="#jobApply{{ $job->id }}">Apply</a></li>
                                <li><span>{{ $job->type->type_name }}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @push('apply-modal')
                    @include('jobs.apply-modal')
                @endpush
                @empty
                @endforelse
            </div>
            <div class="row">
                @if(count($jobs->get()) > 4)
                    <div class="button wow fadeInUp my-5 text-center" data-wow-delay=".8s">
                        <a href="{{ route('jobs.home') }}" class="btn btn-primary"> View More <i class="lni lni-arrow-right"></i></a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endif
@endsection

<script>
    function searchCat(keyword)
    {
        document.querySelector('#hiddenkeyword').value = keyword;
        // alert('keyword');
        document.querySelector('#cat-filter-form').submit();
    }
</script>
