<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Title of The Page -->
    <title>@yield('title') | {{ $appName }}</title>
    <!-- Meta Informations -->
    <meta charset="utf-8">
    <meta name="description" content="Bootstrap Responsive  Avs- Job Portal HTML Template">
    <meta name="keywords" content="@yield('title') | {{ $appName }}">
    <meta name="author" content="@yield('title') | {{ $appName }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @include('inc.styles')

</head>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='../../../embed.tawk.to/61bb14aec82c976b71c1bafa/1fn1chjh0.js';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<body>

<div id="loading-area"></div>
@include('inc.navbar')

@yield('content')
<div class="modal fade form-modal" id="login" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog max-width-px-840 position-relative">
        <button type="button" class="
            circle-32
            btn-reset
            bg-white
            pos-abs-tr
            mt-md-n6
            mr-lg-n6
            focus-reset
            z-index-supper
          " data-dismiss="modal">
            <i class="lni lni-close"></i>
        </button>
        <div class="login-modal-main">
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="row">
                        <div class="heading">
                            <h3>Login From Here</h3>
                            <p>
                                Log in to continue your account <br />
                                and explore new jobs.
                            </p>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email" class="label">E-mail</label>
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@gmail.com" id="email" />

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password" class="label">Password</label>
                                <div class="position-relative">
                                    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Enter password" />
                                </div>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <b>{{ $message }}</b>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group d-flex flex-wrap justify-content-between">
                                <div class="form-check">
                                    <input class="form-check-input" name="remember" type="checkbox"  id="flexCheckDefault" />
                                    <label class="form-check-label" for="flexCheckDefault">Remember password</label>
                                </div>
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}" class="font-size-3 text-dodger line-height-reset">Forget Password</a>
                                @endif
                            </div>
                            <div class="form-group mb-8 button">
                                <button type="submit" class="btn">Log in</button>
                            </div>
                            <p class="text-center create-new-account">
                                Don’t have an account? <a href="#signup">Create a free account</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade form-modal" id="signup" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog max-width-px-840 position-relative">
        <button type="button" class="
            circle-32
            btn-reset
            bg-white
            pos-abs-tr
            mt-md-n6
            mr-lg-n6
            focus-reset
            z-index-supper
          " data-dismiss="modal">
            <i class="lni lni-close"></i>
        </button>
        <div class="login-modal-main">
            <div class="row no-gutters">
                <div class="col-12">
                    <div class="row">
                        <div class="heading">
                            <h3>
                                Create a free Account <br />
                                Today
                            </h3>
                            <p>
                                Create your account to continue <br />
                                and explore new jobs.
                            </p>
                        </div>
                        <div class="social-login">
                            <ul>
                                <li>
                                    <a class="linkedin" href="#"><i class="lni lni-linkedin-original"></i> Import from
                                        LinkedIn</a>
                                </li>
                                <li>
                                    <a class="google" href="#"><i class="lni lni-google"></i> Import from Google</a>
                                </li>
                                <li>
                                    <a class="facebook" href="#"><i class="lni lni-facebook-original"></i> Import from
                                        Facebook</a>
                                </li>
                            </ul>
                        </div>
                        <div class="or-devider">
                            <span>Or</span>
                        </div>
                        <form >
                            <div class="form-group">
                                <label for="email" class="label">E-mail</label>
                                <input type="email" class="form-control" placeholder="example@gmail.com" />
                            </div>
                            <div class="form-group">
                                <label for="password" class="label">Password</label>
                                <div class="position-relative">
                                    <input type="password" class="form-control" placeholder="Enter password" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="label">Confirm Password</label>
                                <div class="position-relative">
                                    <input type="password" class="form-control" placeholder="Enter password" />
                                </div>
                            </div>
                            <div class="form-group d-flex flex-wrap justify-content-between">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" />
                                    <label class="form-check-label" for="flexCheckDefault">Agree to the <a href="#">Terms &
                                            Conditions</a></label>
                                </div>
                            </div>
                            <div class="form-group mb-8 button">
                                <button class="btn">Sign Up</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('inc.footer')

<a href="#" class="scroll-top btn-hover">
    <i class="lni lni-chevron-up"></i>
</a>
@include('inc.scripts')
@stack('apply-modal')
</body>

</html>
