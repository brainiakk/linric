@extends('layouts.master')
@section('title', 'My Jobs')
@section('content')
@include('inc.dash-bread')
<section>
    <div class="block no-padding">
        <div class="container">
             <div class="row no-gape">
                 @include('inc.sidebar')
                 <div class="col-lg-9 column">
                     <div class="padding-left">
                         <div class="manage-jobs-sec">
                             <h3>@yield('title')</h3>
                             <table>
                                 @if (count($jobs) > 0)

                                 <thead>
                                     <tr>
                                         <td>Job Title</td>
                                         <td>Status</td>
                                         <td>Date</td>
                                         <td></td>
                                     </tr>
                                 </thead>
                                 @endif
                                 <tbody>
                                     @forelse ($jobs as $job)
                                     <tr>
                                         <td>
                                             <div class="table-list-title">
                                                 <i>{{ $job->job->title }} / {{ $job->job->category->cat_name }}</i><br>
                                                 <span><i class="la la-map-marker"></i>{{ $job->job->location }}</span>
                                             </div>
                                         </td>
                                         <td>
                                             <div class="table-list-title">
                                                @if ($job->status == '1')
                                                    <span class="text-warning bold" style="font-weight: bold;">Pending Approval</span>
                                                @elseif ($job->status == '2')
                                                    <span class="text-success bold" style="font-weight: bold;">Approved</span>
                                                @else
                                                    <span class="text-danger bold" style="font-weight: bold;">Rejected</span>
                                                @endif
                                             </div>
                                         </td>
                                         <td>
                                             <span>{{ $job->created_at->format('M d, Y') }}</span><br>
                                         </td>
                                         <td>
                                             <ul class="action_job">
                                                 <li><span>Terminate Job</span><a onclick="confirm('Are You Sure You want to terminate employment?\n If Yes click OK or Cancel.');" href="{{ route('dashboard.jobs.assigned.delete', $job->id) }}" title=""><i class="la la-trash-o"></i></a></li>
                                             </ul>
                                         </td>
                                     </tr>
                                     @empty
                                    <div class="alert alert-warning block d-flex justify-content-between">
                                        <h3>Oops!! You currently have no job assigned to you.</h3>
                                        <a href="{{ route('jobs.home') }}" class="btn btn-success"> Find Jobs <i class="bx bx-right-arrow-alt" style="vertical-align: middle;"></i></a>
                                    </div>
                                     @endforelse
                                 </tbody>
                             </table>
                         </div>
                     </div>
                </div>
             </div>
        </div>
    </div>
</section>
@endsection
