@extends('layouts.master')
@section('title', 'Leave Requests')
@section('content')
@include('inc.dash-bread')
<section>
    <div class="block no-padding">
        <div class="container">
             <div class="row no-gape">
                 @include('inc.sidebar')
                 <div class="col-lg-9 column">
                     <div class="padding-left">
                         <div class="manage-jobs-sec">
                             <h3>@yield('title')</h3>
                             <table class="alrt-table">
                                 <thead>
                                     <tr>
                                         <td>Leave Details</td>
                                         <td class="text-right"><button class="btn btn-success"  data-toggle="modal" data-target="#exampleModalCenter">Apply For Leave <i class="bx bx-plus align-middle"></i></button></td>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     @forelse ($leaves as $leave)
                                     <tr>
                                         <td>
                                             <div class="table-list-title">
                                                 <h3><a href="#" title="">{{ $leave->title }}</a> &nbsp; <small style="font-weight: bold;"><i class="bx bx-calendar-event"></i> {{ $leave->start->format('d M, Y').' - '.$leave->end->format('d M, Y') }}</small></h3>
                                                 <span>{{ $leave->details }}</span>
                                             </div>
                                         </td>
                                         <td>
                                             <ul class="action_job">
                                                 <li><span>Delete</span><a href="{{ route('dashboard.leave.delete', $leave->id) }}" title=""><i class="bx bx-x-circle text-danger bx-sm"></i></a></li>
                                             </ul>
                                             @if ($leave->status == '1')
                                             <span class="badge badge-warning text-white">Pending Approval</span>
                                             @elseif ($leave->status == '2')
                                             <span class="badge badge-success text-white">Approved</span>
                                             @else
                                             <span class="badge badge-danger text-white">Rejected</span>
                                             @endif
                                         </td>
                                     </tr>
                                     @empty
                                        <tr>
                                            <td style="width: 100%;">
                                                <div class="table-list-title">
                                                    <h4 class="alert alert-warning">No Leave Request Found!!</h4>
                                                </div>
                                            </td>
                                        </tr>
                                     @endforelse
                                 </tbody>
                             </table>
                         </div>
                     </div>
                </div>
             </div>
        </div>
    </div>
</section>
@endsection
