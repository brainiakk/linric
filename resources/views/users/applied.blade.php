@extends('layouts.master')
@section('title', 'Jobs Applications')
@section('content')
@include('inc.breadcrumb')
<div class="manage-applications section">
    <div class="container">
        <div class="alerts-inner">
            <div class="row">
                @include('inc.dashboard.sidebar')
                <div class="col-lg-8 col-12">
                    <div class="job-items">
                        @forelse ($applied as $app)
                        <div class="manage-content">
                            <div class="row align-items-center justify-content-center">
                                <div class="col-lg-4 col-md-4">
                                    <div class="title-img">
                                        <h3>
                                            {{ $app->job->title }} <span>{{ $app->job->category->cat_name }}</span>
                                        </h3>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <p><span class="time">{{ $app->job->type->type_name }}</span></p>
                                </div>
                                <div class="col-lg-3 col-md-3">
                                    <p>

                                        @if($app->status == '1')
                                            Pending
                                        @elseif($app->status == '2')
                                            Approved
                                        @else
                                            Rejected
                                        @endif
                                    </p>
                                </div>
                                @if($app->user_id ==  auth()->id())
                                <div class="col-lg-2 col-md-2">
                                        <a href="#" onclick=" event.preventDefault(); document.querySelector('#delete{{$app->id}}').click();">
                                            <i class="lni lni-trash"></i>
                                        </a>
                                </div>
                                @endif
                            </div>
                            <a href="{{ route('dashboard.jobs.applied.delete', $app->id) }}" id="delete{{$app->id}}"></a>
                        </div>
                        @empty
                            <div class="manage-content">
                                <div class="row align-items-center justify-content-center">
                                    <div class="col-lg-12">
                                        <div class="title-img">
                                            <h3>Oops!! You have no job applications at this time.</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforelse
                    </div>

                    <div class="pagination left pagination-md-center">
                        <ul class="pagination-list">
                            {!! $applied->links() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

