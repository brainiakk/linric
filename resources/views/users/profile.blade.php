@extends('layouts.master')
@section('title', $user->name())
@section('content')
    @include('inc.breadcrumb')
    <section class="job-post section">
        <div class="container">
            <div class="row">
                @include('inc.dashboard.sidebar')
                <div class="col-lg-8 col-12">
                    <div class="job-information">
                        <h3 class="title">My Profile</h3>
                        @include('inc.dashboard.alert')
                            <form class="col-lg-12 col-md-12" method="POST" action="{{ route('dashboard.profile.update', $user->user_id) }}">
                                @method('patch')
                                @csrf
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>First Name*</label>
                                                    <input class="form-control" type="text"  name="fname"  value="{{ old('fname', $user->fname) }}"/>
                                                </div>
                                                @error('fname')
                                                <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Middle Name</label>
                                                    <input class="form-control" type="text"  name="mname"  value="{{ old('mname', $user->mname) }}"/>
                                                </div>
                                                @error('mname')
                                                <span class="invalid-feedback" role="alert">
                                            <b>{{ $message }}</b>
                                        </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>Last Name*</label>
                                                    <input class="form-control" type="text"  name="lname"  value="{{ old('lname', $user->lname) }}"/>
                                                </div>
                                                @error('lname')
                                                <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Email Address*</label>
                                                    <input class="form-control" type="email"  name="email"  value="{{ old('email', $user->email) }}"/>
                                                </div>
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Gender*</label>
                                                    <select class="select" name="gender">
                                                        <option value=""></option>
                                                        <option value="f" @if($user->gender == 'f') selected @endif>Female</option>
                                                        <option value="m" @if($user->gender == 'm') selected @endif>Male</option>
                                                    </select>
                                                </div>
                                                @error('gender')
                                                <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group">
                                                    <label>Telephone Number</label>
                                                    <div class="input-group date">
                                                        <input type="tel" class="form-control" name="tel" value="{{ old('tel', $user->tel) }}"  placeholder="929293290"/>
                                                    </div>
                                                </div>
                                                @error('tel')
                                                <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 button">
                                                <button type="submit" class="btn">Update Profile </button>
                                            </div>
                                        </div>
                                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
