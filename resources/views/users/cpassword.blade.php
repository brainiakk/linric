@extends('layouts.master')
@section('title', 'Change Password')
@section('content')
    @include('inc.breadcrumb')
    <div class="change-password section">
        <div class="container">
            <div class="alerts-inner">
                <div class="row">
                    @include('inc.dashboard.sidebar')

                    <div class="col-lg-8">
                        <div class="password-content">
                            <h3>Change Password</h3>
                            <p>Here you can change your password please fill up the form.</p>
                            <form method="POST" action="{{ route('dashboard.password.change') }}">
                            @csrf
                            <!-- Table-->
                                @include('inc.dashboard.alert')
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Old Password</label>
                                            <input class="form-control" type="password">
                                        </div>

                                        @error('current_password')
                                        <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input class="form-control" type="password"  name="new_password"/>
                                            <i class="bx bxs-low-vision"></i>
                                        </div>

                                        @error('new_password')
                                        <span class="invalid-feedback" role="alert">
                                                    <b>{{ $message }}</b>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group last">
                                            <label>Confirm Password</label>
                                            <input class="form-control" type="password" name="new_confirm_password"/>
                                            <i class="bx bxs-low-vision"></i>
                                        </div>

                                        @error('new_confirm_password')
                                        <span class="invalid-feedback" role="alert" >
                                                    <b>{{ $message }}</b>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="button">
                                            <button type="submit" class="btn">Save Change</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
