@extends('layouts.master')
@section('title', 'Account Settiings')
@section('content')
@include('inc.dash-bread')
<section>
    <div class="block no-padding">
        <div class="container">
             <div class="row no-gape">
                 @include('inc.sidebar')
                 <div class="col-lg-9 column">
                     <div class="padding-left">
                         <div class="manage-jobs-sec">
                             <div class="change-password">

                                 <form class="needs-validation col-md-6" method="POST" action="{{ route('dashboard.bank.store') }}">

                                     <div class="row">
                                         <div class="col-md-12 mt-2"><h4>Bank Details</h4></div>
                                         @csrf
                                         <div class="col-md-12">
                                             <span class="pf-title">Account Name*</span>
                                             <div class="pf-field">
                                                 <input type="text" value="{{ old('account_name', $bank->account_name) }}" name="account_name" required/>
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <span class="pf-title">Account Number*</span>
                                             <div class="pf-field">
                                                 <input type="text" value="{{ old('account_number', $bank->account_number) }}" name="account_number" required/>
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <span class="pf-title">Bank Name*</span>
                                             <div class="pf-field">
                                                 <input type="text" value="{{ old('bank_name', $bank->bank_name) }}" name="bank_name" required/>
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <span class="pf-title">Bank Routing Number*</span>
                                             <div class="pf-field">
                                                 <input type="text" value="{{ old('routing_number', $bank->routing_number) }}" name="routing_number" required/>
                                             </div>
                                         </div>
                                         <div class="col-md-12 mb-5">
                                             <button type="submit">Save</button>
                                         </div>
                                     </div>
                                 </form>
                                 <form class="needs-validation col-md-6" method="POST" action="{{ route('dashboard.document.store') }}" enctype="multipart/form-data">

                                     <div class="row">
                                         <div class="col-md-12 mt-2"><h4>My Documents</h4></div>
                                         @csrf
                                         <div class="col-md-12">
                                             <span class="pf-title">Resume<span class="text-danger">*</span></span>
                                             <div class="pf-field">
                                                 <input type="file" value="{{ old('resume', $doc->resume) }}" name="resume" />
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <span class="pf-title">SSN Card<span class="text-danger">*</span></span>
                                             <div class="pf-field">
                                                 <input type="file" value="{{ old('ssn', $doc->ssn) }}" name="ssn" />
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <span class="pf-title">Valid ID Card<span class="text-danger">*</span></span>
                                             <div class="pf-field">
                                                 <input type="file" value="{{ old('id', $doc->id) }}" name="id" />
                                             </div>
                                         </div>
                                         <div class="col-md-12">
                                             <span class="pf-title">Passport<span class="text-danger">*</span></span>
                                             <div class="pf-field">
                                                 <input type="file" value="{{ old('passport', $doc->passport) }}" name="passport" />
                                             </div>
                                         </div>
                                         <div class="col-md-12 mb-5">
                                             <button type="submit">Upload</button>
                                         </div>
                                     </div>
                                 </form>
                                 <div class="col-12">
                                    <div class="container">
                                        <h4>Deactivate Account</h4>
                                        <a href="{{ route('dashboard.deactivate.account', \Auth::id()) }}" class="btn btn-danger mt-2" onclick="confirm('Are You Sure?\n If Yes click OK or Cancel.');">Close Account <i class='bx bx-user-x' style="vertical-align: middle;"></i></a>
                                        <div class="mb-3"><small class="text-danger ">You won't be able to login once account is closed</small></div>
                                    </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                </div>
             </div>
        </div>
    </div>
</section>
@endsection
